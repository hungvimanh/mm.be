﻿using System;
using System.Collections.Generic;

namespace MM.Models
{
    public partial class AccountDAO
    {
        public AccountDAO()
        {
            AccountFoodFavorites = new HashSet<AccountFoodFavoriteDAO>();
            Comments = new HashSet<CommentDAO>();
            FoodAccountMappings = new HashSet<FoodAccountMappingDAO>();
            Notifications = new HashSet<NotificationDAO>();
            Orders = new HashSet<OrderDAO>();
        }

        public long Id { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string PasswordRecoveryCode { get; set; }
        public DateTime? ExpiredTimeCode { get; set; }
        public string Address { get; set; }
        public DateTime? Dob { get; set; }
        public long? ImageId { get; set; }
        public long RoleId { get; set; }
        public string FacebookId { get; set; }
        public string GoogleId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public virtual ImageDAO Image { get; set; }
        public virtual ICollection<AccountFoodFavoriteDAO> AccountFoodFavorites { get; set; }
        public virtual ICollection<CommentDAO> Comments { get; set; }
        public virtual ICollection<FoodAccountMappingDAO> FoodAccountMappings { get; set; }
        public virtual ICollection<NotificationDAO> Notifications { get; set; }
        public virtual ICollection<OrderDAO> Orders { get; set; }
    }
}
