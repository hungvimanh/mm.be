﻿using System;
using System.Collections.Generic;

namespace MM.Models
{
    public partial class CommentDAO
    {
        public long Id { get; set; }
        public long FoodId { get; set; }
        public string Content { get; set; }
        public DateTime Time { get; set; }
        public long AccountId { get; set; }

        public virtual AccountDAO Account { get; set; }
        public virtual FoodDAO Food { get; set; }
    }
}
