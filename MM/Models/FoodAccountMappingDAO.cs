﻿using System;
using System.Collections.Generic;

namespace MM.Models
{
    public partial class FoodAccountMappingDAO
    {
        public long FoodId { get; set; }
        public long AccountId { get; set; }
        public long? Rate { get; set; }

        public virtual AccountDAO Account { get; set; }
        public virtual FoodDAO Food { get; set; }
    }
}
