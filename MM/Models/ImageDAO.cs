﻿using System;
using System.Collections.Generic;

namespace MM.Models
{
    public partial class ImageDAO
    {
        public ImageDAO()
        {
            Accounts = new HashSet<AccountDAO>();
            Foods = new HashSet<FoodDAO>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
        public string MimeType { get; set; }
        public string Url { get; set; }
        public string Path { get; set; }

        public virtual ICollection<AccountDAO> Accounts { get; set; }
        public virtual ICollection<FoodDAO> Foods { get; set; }
    }
}
