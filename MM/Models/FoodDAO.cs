﻿using System;
using System.Collections.Generic;

namespace MM.Models
{
    public partial class FoodDAO
    {
        public FoodDAO()
        {
            AccountFoodFavorites = new HashSet<AccountFoodFavoriteDAO>();
            Comments = new HashSet<CommentDAO>();
            FoodAccountMappings = new HashSet<FoodAccountMappingDAO>();
            FoodFoodGroupingMappings = new HashSet<FoodFoodGroupingMappingDAO>();
            FoodFoodTypeMappings = new HashSet<FoodFoodTypeMappingDAO>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public decimal PriceEach { get; set; }
        public decimal? DiscountRate { get; set; }
        public long? ImageId { get; set; }
        public long StatusId { get; set; }
        public string Descreption { get; set; }
        public decimal Rate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public virtual ImageDAO Image { get; set; }
        public virtual ICollection<AccountFoodFavoriteDAO> AccountFoodFavorites { get; set; }
        public virtual ICollection<CommentDAO> Comments { get; set; }
        public virtual ICollection<FoodAccountMappingDAO> FoodAccountMappings { get; set; }
        public virtual ICollection<FoodFoodGroupingMappingDAO> FoodFoodGroupingMappings { get; set; }
        public virtual ICollection<FoodFoodTypeMappingDAO> FoodFoodTypeMappings { get; set; }
    }
}
