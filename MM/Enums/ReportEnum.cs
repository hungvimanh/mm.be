﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Enums
{
    public class ReportEnum
    {
        public static GenericEnum YEAR = new GenericEnum { Id = 1, Code = "YEAR", Name = "Theo năm" };
        public static GenericEnum MONTH = new GenericEnum { Id = 2, Code = "MONTH", Name = "Theo tháng" };
    }
}
