﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Enums
{
    public class FoodTypeEnums
    {
        public static GenericEnum SMALL = new GenericEnum { Id = 1, Code = "Small", Name = "Size nhỏ" };
        public static GenericEnum MEDIUM = new GenericEnum { Id = 2, Code = "Medium", Name = "Size vừa" };
        public static GenericEnum LARGE = new GenericEnum { Id = 3, Code = "Large", Name = "Size lớn" };
    }
}
