﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Enums
{
    public class NotificationStatusEnum
    {
        public static bool READ = true;
        public static bool UNREAD = false;
    }
}
