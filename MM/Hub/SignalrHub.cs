﻿using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Services
{
    //public class SignalrHub : Hub
    //{
    //    public async Task SendMessage(string user, string message)
    //    {
    //        await Clients.All.SendAsync("sendToProvider", user, message);
    //    }

    //    [HubMethodName("receivedFromCustomer")]
    //    public Task ReplyUser(string message)
    //    {
    //        return Clients.All.SendAsync("sendToProvider", message);
    //    }
    //}

    [Authorize]
    public class SignalrHub : Microsoft.AspNetCore.SignalR.Hub
    {
    }
}
