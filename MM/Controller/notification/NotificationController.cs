using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using MM.Entities;
using MM.Services;
using MM.Services.MAccount;
using MM.Services.MNotification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.notification
{
    public class NotificationRoute : Root
    {
        private const string Default = Api + "/Notification";
        public const string Count = Default + "/count";
        public const string List = Default + "/list";
        public const string Get = Default + "/get";
        public const string Create = Default + "/create";
        public const string Update = Default + "/update";
        public const string Delete = Default + "/delete";
        public const string BulkDelete = Default + "/bulk-delete";
    }

    public class NotificationController : ApiController
    {
        private IAccountService AccountService;
        private INotificationService NotificationService;
        private ICurrentContext CurrentContext;
        protected IHubContext<SignalrHub> signalRcontext;
        public NotificationController(
            IAccountService AccountService,
            INotificationService NotificationService,
            ICurrentContext CurrentContext,
            IHubContext<SignalrHub> SignalrHub
        )
        {
            this.AccountService = AccountService;
            this.NotificationService = NotificationService;
            this.CurrentContext = CurrentContext;
            this.signalRcontext = SignalrHub;
        }

        [Route(NotificationRoute.Count), HttpPost]
        public async Task<ActionResult<int>> Count([FromBody] Notification_NotificationFilterDTO Notification_NotificationFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            NotificationFilter NotificationFilter = ConvertFilterDTOToFilterEntity(Notification_NotificationFilterDTO);
            int count = await NotificationService.Count(NotificationFilter);
            return count;
        }

        [Route(NotificationRoute.List), HttpPost]
        public async Task<ActionResult<List<Notification_NotificationDTO>>> List([FromBody] Notification_NotificationFilterDTO Notification_NotificationFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            NotificationFilter NotificationFilter = ConvertFilterDTOToFilterEntity(Notification_NotificationFilterDTO);
            List<Notification> Notifications = await NotificationService.List(NotificationFilter);
            List<Notification_NotificationDTO> Notification_NotificationDTOs = Notifications
                .Select(c => new Notification_NotificationDTO(c)).ToList();
            return Notification_NotificationDTOs;
        }

        [Route(NotificationRoute.Get), HttpPost]
        public async Task<ActionResult<Notification_NotificationDTO>> Get([FromBody]Notification_NotificationDTO Notification_NotificationDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Notification Notification = await NotificationService.Get(Notification_NotificationDTO.Id);
            return new Notification_NotificationDTO(Notification);
        }

        [Route(NotificationRoute.Create), HttpPost]
        public async Task<ActionResult<Notification_NotificationDTO>> Create([FromBody] Notification_NotificationDTO Notification_NotificationDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Notification Notification = ConvertDTOToEntity(Notification_NotificationDTO);
            var User = await AccountService.Get(CurrentContext.AccountId);
            try
            {
                await signalRcontext.Clients.All.SendAsync("sendToProvider", User.DisplayName, Notification.Content);
                Notification = await NotificationService.Create(Notification);
                Notification_NotificationDTO = new Notification_NotificationDTO(Notification);
                if (Notification.IsValidated)
                    return Notification_NotificationDTO;
                else
                    return BadRequest(Notification_NotificationDTO);
            }
            catch
            {
                return BadRequest("Can not send message to provider!");
            }
        }

        [Route(NotificationRoute.Update), HttpPost]
        public async Task<ActionResult<Notification_NotificationDTO>> Update([FromBody] Notification_NotificationDTO Notification_NotificationDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);
            
            Notification Notification = ConvertDTOToEntity(Notification_NotificationDTO);
            Notification = await NotificationService.Update(Notification);
            Notification_NotificationDTO = new Notification_NotificationDTO(Notification);
            if (Notification.IsValidated)
                return Notification_NotificationDTO;
            else
                return BadRequest(Notification_NotificationDTO);
        }

        [Route(NotificationRoute.Delete), HttpPost]
        public async Task<ActionResult<Notification_NotificationDTO>> Delete([FromBody] Notification_NotificationDTO Notification_NotificationDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Notification Notification = ConvertDTOToEntity(Notification_NotificationDTO);
            Notification = await NotificationService.Delete(Notification);
            Notification_NotificationDTO = new Notification_NotificationDTO(Notification);
            if (Notification.IsValidated)
                return Notification_NotificationDTO;
            else
                return BadRequest(Notification_NotificationDTO);
        }

        [Route(NotificationRoute.BulkDelete), HttpPost]
        public async Task<ActionResult<bool>> BulkDelete()
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            NotificationFilter NotificationFilter = new NotificationFilter();
            NotificationFilter.Selects = NotificationSelect.Id;
            NotificationFilter.Skip = 0;
            NotificationFilter.Take = int.MaxValue;

            List<Notification> Notifications = await NotificationService.List(NotificationFilter);
            Notifications = await NotificationService.BulkDelete(Notifications);
            if (Notifications.Any(x => !x.IsValidated))
                return BadRequest(Notifications.Where(x => !x.IsValidated));
            return true;
        }

        private Notification ConvertDTOToEntity(Notification_NotificationDTO Notification_NotificationDTO)
        {
            Notification Notification = new Notification();
            Notification.Id = Notification_NotificationDTO.Id;
            Notification.AccountId = Notification_NotificationDTO.AccountId;
            Notification.Content = Notification_NotificationDTO.Content;
            Notification.Time = Notification_NotificationDTO.Time;
            Notification.Unread = Notification_NotificationDTO.Unread;
            Notification.Account = Notification_NotificationDTO.Account == null ? null : new Account
            {
                Id = Notification_NotificationDTO.Account.Id,
                DisplayName = Notification_NotificationDTO.Account.DisplayName,
                Email = Notification_NotificationDTO.Account.Email,
                Phone = Notification_NotificationDTO.Account.Phone,
                Password = Notification_NotificationDTO.Account.Password,
                Salt = Notification_NotificationDTO.Account.Salt,
                PasswordRecoveryCode = Notification_NotificationDTO.Account.PasswordRecoveryCode,
                ExpiredTimeCode = Notification_NotificationDTO.Account.ExpiredTimeCode,
                Address = Notification_NotificationDTO.Account.Address,
                Dob = Notification_NotificationDTO.Account.Dob,
                ImageId = Notification_NotificationDTO.Account.ImageId,
                RoleId = Notification_NotificationDTO.Account.RoleId,
            };
            Notification.BaseLanguage = CurrentContext.Language;
            return Notification;
        }

        private NotificationFilter ConvertFilterDTOToFilterEntity(Notification_NotificationFilterDTO Notification_NotificationFilterDTO)
        {
            NotificationFilter NotificationFilter = new NotificationFilter();
            NotificationFilter.Selects = NotificationSelect.ALL;
            NotificationFilter.Skip = Notification_NotificationFilterDTO.Skip;
            NotificationFilter.Take = Notification_NotificationFilterDTO.Take;
            NotificationFilter.OrderBy = NotificationOrder.Time;
            NotificationFilter.OrderType = OrderType.DESC;

            NotificationFilter.Id = Notification_NotificationFilterDTO.Id;
            NotificationFilter.AccountId = Notification_NotificationFilterDTO.AccountId;
            NotificationFilter.Content = Notification_NotificationFilterDTO.Content;
            NotificationFilter.Time = Notification_NotificationFilterDTO.Time;
            return NotificationFilter;
        }

    }
}

