using Common;
using System;
using System.Linq;
using System.Collections.Generic;
using MM.Entities;

namespace MM.Controller.notification
{
    public class Notification_AccountDTO : DataDTO
    {
        
        public long Id { get; set; }
        
        public string DisplayName { get; set; }
        
        public string Email { get; set; }
        
        public string Phone { get; set; }
        
        public string Password { get; set; }
        
        public string Salt { get; set; }
        
        public string PasswordRecoveryCode { get; set; }
        
        public DateTime? ExpiredTimeCode { get; set; }
        
        public string Address { get; set; }
        
        public DateTime? Dob { get; set; }
        
        public long? ImageId { get; set; }
        
        public long? SexId { get; set; }
        
        public long StatusId { get; set; }
        
        public long RoleId { get; set; }
        public Notification_ImageDTO Image { get; set; }

        public Notification_AccountDTO() {}
        public Notification_AccountDTO(Account Account)
        {
            
            this.Id = Account.Id;
            
            this.DisplayName = Account.DisplayName;
            
            this.Email = Account.Email;
            
            this.Phone = Account.Phone;
            
            this.Password = Account.Password;
            
            this.Salt = Account.Salt;
            
            this.PasswordRecoveryCode = Account.PasswordRecoveryCode;
            
            this.ExpiredTimeCode = Account.ExpiredTimeCode;
            
            this.Address = Account.Address;
            
            this.Dob = Account.Dob;
            
            this.ImageId = Account.ImageId;
            this.Image = Account.Image == null ? null : new Notification_ImageDTO(Account.Image);


            this.RoleId = Account.RoleId;
            
            this.Errors = Account.Errors;
        }
    }

    public class Notification_AccountFilterDTO : FilterDTO
    {
        
        public IdFilter Id { get; set; }
        
        public StringFilter DisplayName { get; set; }
        
        public StringFilter Email { get; set; }
        
        public StringFilter Phone { get; set; }
        
        public StringFilter Password { get; set; }
        
        public StringFilter Salt { get; set; }
        
        public StringFilter PasswordRecoveryCode { get; set; }
        
        public DateFilter ExpiredTimeCode { get; set; }
        
        public StringFilter Address { get; set; }
        
        public DateFilter Dob { get; set; }
        
        
        
        public IdFilter RoleId { get; set; }
        
        public AccountOrder OrderBy { get; set; }
    }
}