using Common;
using System;
using System.Linq;
using System.Collections.Generic;
using MM.Entities;

namespace MM.Controller.notification
{
    public class Notification_NotificationDTO : DataDTO
    {
        public long Id { get; set; }
        public long AccountId { get; set; }
        public string Content { get; set; }
        public DateTime Time { get; set; }
        public bool Unread { get; set; }
        public Notification_AccountDTO Account { get; set; }
        public Notification_NotificationDTO() {}
        public Notification_NotificationDTO(Notification Notification)
        {
            this.Id = Notification.Id;
            this.AccountId = Notification.AccountId;
            this.Content = Notification.Content;
            this.Time = Notification.Time;
            this.Unread = Notification.Unread;
            this.Account = Notification.Account == null ? null : new Notification_AccountDTO(Notification.Account);
            this.Errors = Notification.Errors;
        }
    }

    public class Notification_NotificationFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public IdFilter AccountId { get; set; }
        public StringFilter Content { get; set; }
        public DateFilter Time { get; set; }
        public NotificationOrder OrderBy { get; set; }
    }
}
