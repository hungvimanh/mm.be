using Common;
using System;
using System.Linq;
using System.Collections.Generic;
using MM.Entities;
using MM.Enums;

namespace MM.Controller.table
{
    public class Table_TableDTO : DataDTO
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public long StatusId { get; set; }
        public Table_StatusDTO Status
        {
            get
            {
                if (this.StatusId == StatusEnum.ACTIVE.Id)
                    return new Table_StatusDTO
                    {
                        Id = StatusEnum.ACTIVE.Id,
                        Code = StatusEnum.ACTIVE.Code,
                        Name = StatusEnum.ACTIVE.Name,
                    };
                if (this.StatusId == StatusEnum.INACTIVE.Id)
                    return new Table_StatusDTO
                    {
                        Id = StatusEnum.INACTIVE.Id,
                        Code = StatusEnum.INACTIVE.Code,
                        Name = StatusEnum.INACTIVE.Name,
                    };
                return null;
            }
        }
        public Table_TableDTO() {}
        public Table_TableDTO(Table Table)
        {
            this.Id = Table.Id;
            this.Code = Table.Code;
            this.StatusId = Table.StatusId;
            this.Errors = Table.Errors;
        }
    }

    public class Table_TableFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public StringFilter Code { get; set; }
        public IdFilter StatusId { get; set; }
        public TableOrder OrderBy { get; set; }
    }
}
