using Common;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MM.Entities;
using MM.Enums;
using MM.Services.MOrder;
using MM.Services.MReservation;
using MM.Services.MTable;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.table
{
    public class TableRoute : Root
    {
        private const string Default = Api + "/table";
        public const string Count = Default + "/count";
        public const string List = Default + "/list";
        public const string Get = Default + "/get";
        public const string Create = Default + "/create";
        public const string Update = Default + "/update";
        public const string Delete = Default + "/delete";
        public const string Init = Default + "/init";
    }

    public class TableController : ApiController
    {
        private ITableService TableService;
        private IReservationService ReservationService;
        private ICurrentContext CurrentContext;
        public TableController(
            ITableService TableService,
            IReservationService ReservationService,
            ICurrentContext CurrentContext
        )
        {
            this.TableService = TableService;
            this.ReservationService = ReservationService;
            this.CurrentContext = CurrentContext;
        }

        [Route(TableRoute.Count), HttpPost]
        public async Task<ActionResult<int>> Count([FromBody] Table_TableFilterDTO Table_TableFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            TableFilter TableFilter = ConvertFilterDTOToFilterEntity(Table_TableFilterDTO);
            int count = await TableService.Count(TableFilter);
            return count;
        }

        [Route(TableRoute.List), HttpPost]
        public async Task<ActionResult<List<Table_TableDTO>>> List([FromBody] Table_TableFilterDTO Table_TableFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            TableFilter TableFilter = ConvertFilterDTOToFilterEntity(Table_TableFilterDTO);
            List<Table> Tables = await TableService.List(TableFilter);
            List<Table_TableDTO> Table_TableDTOs = Tables
                .Select(c => new Table_TableDTO(c)).ToList();
            return Table_TableDTOs;
        }

        [Route(TableRoute.Get), HttpPost]
        public async Task<ActionResult<Table_TableDTO>> Get([FromBody]Table_TableDTO Table_TableDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Table Table = await TableService.Get(Table_TableDTO.Id);
            return new Table_TableDTO(Table);
        }

        [Route(TableRoute.Create), HttpPost]
        public async Task<ActionResult<Table_TableDTO>> Create([FromBody] Table_TableDTO Table_TableDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Table Table = ConvertDTOToEntity(Table_TableDTO);
            Table = await TableService.Create(Table);
            Table_TableDTO = new Table_TableDTO(Table);
            if (Table.IsValidated)
                return Table_TableDTO;
            else
                return BadRequest(Table_TableDTO);
        }

        [Route(TableRoute.Update), HttpPost]
        public async Task<ActionResult<Table_TableDTO>> Update([FromBody] Table_TableDTO Table_TableDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Table Table = ConvertDTOToEntity(Table_TableDTO);
            Table = await TableService.Update(Table);
            Table_TableDTO = new Table_TableDTO(Table);
            if (Table.IsValidated)
                return Table_TableDTO;
            else
                return BadRequest(Table_TableDTO);
        }

        [Route(TableRoute.Delete), HttpPost]
        public async Task<ActionResult<Table_TableDTO>> Delete([FromBody] Table_TableDTO Table_TableDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Table Table = ConvertDTOToEntity(Table_TableDTO);
            Table = await TableService.Delete(Table);
            Table_TableDTO = new Table_TableDTO(Table);
            if (Table.IsValidated)
                return Table_TableDTO;
            else
                return BadRequest(Table_TableDTO);
        }
        
        private Table ConvertDTOToEntity(Table_TableDTO Table_TableDTO)
        {
            Table Table = new Table();
            Table.Id = Table_TableDTO.Id;
            Table.Code = Table_TableDTO.Code;
            Table.StatusId = Table_TableDTO.StatusId;
            Table.BaseLanguage = CurrentContext.Language;
            return Table;
        }

        private TableFilter ConvertFilterDTOToFilterEntity(Table_TableFilterDTO Table_TableFilterDTO)
        {
            TableFilter TableFilter = new TableFilter();
            TableFilter.Selects = TableSelect.ALL;
            TableFilter.Skip = Table_TableFilterDTO.Skip;
            TableFilter.Take = Table_TableFilterDTO.Take;
            TableFilter.OrderBy = Table_TableFilterDTO.OrderBy;
            TableFilter.OrderType = Table_TableFilterDTO.OrderType;

            TableFilter.Id = Table_TableFilterDTO.Id;
            TableFilter.Code = Table_TableFilterDTO.Code;
            TableFilter.StatusId = Table_TableFilterDTO.StatusId;
            return TableFilter;
        }
        //[AllowAnonymous]
        //[Route(TableRoute.Init), HttpGet]
        public async Task<ActionResult> Init()
        {
            ReservationFilter ReservationFilter = new ReservationFilter
            {
                Skip = 0,
                Take = int.MaxValue,
                Selects = ReservationSelect.Id
            };

            List<Reservation> Reservations = await ReservationService.List(ReservationFilter);
            await ReservationService.BulkDelete(Reservations);

            TableFilter filter = new TableFilter
            {
                Skip = 0,
                Take = int.MaxValue,
                Selects = TableSelect.Id
            };

            var tables = await TableService.List(filter);
            Reservations = new List<Reservation>();
            foreach (var table in tables)
            {
                for (int i = 0; i < 40; i++)
                {
                    Reservation Reservation = new Reservation();
                    Reservation.TableId = table.Id;
                    Reservation.Date = StaticParams.DateTimeNow.AddDays(i);
                    Reservation.StatusId = ReservationStatusEnum.EMPTY.Id;
                    Reservations.Add(Reservation);
                }
            }

            await ReservationService.BulkInsert(Reservations);
            return Ok();
        }

    }
}

