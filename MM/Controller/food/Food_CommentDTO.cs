﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.food
{
    public class Food_CommentDTO : DataDTO
    {
        public long Id { get; set; }
        public long FoodId { get; set; }
        public string Content { get; set; }
        public DateTime Time { get; set; }
        public long AccountId { get; set; }
        public Food_FoodAccountMappingDTO FoodAccountMapping { get; set; }
        public Food_AccountDTO Account { get; set; }
        public Food_CommentDTO() { }
        public Food_CommentDTO(Comment Comment)
        {
            this.Id = Comment.Id;
            this.FoodId = Comment.FoodId;
            this.Content = Comment.Content;
            this.Time = Comment.Time;
            this.AccountId = Comment.AccountId;
            this.Account = Comment.Account == null ? null : new Food_AccountDTO(Comment.Account);
            this.FoodAccountMapping = Comment.FoodAccountMapping == null ? null : new Food_FoodAccountMappingDTO(Comment.FoodAccountMapping);
        }
    }
}
