﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.food
{
    public class Food_AccountDTO : DataDTO
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public long? ImageId { get; set; }
        public Food_ImageDTO Image { get; set; }
        public Food_AccountDTO() { }
        public Food_AccountDTO(Account Account)
        {
            this.Id = Account.Id;
            this.Email = Account.Email;
            this.DisplayName = Account.DisplayName;
            this.ImageId = Account.ImageId;
            this.Image = Account.Image == null ? null : new Food_ImageDTO(Account.Image);
            this.Errors = Account.Errors;
        }
    }
}
