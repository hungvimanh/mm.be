using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MM.Entities;
using MM.Enums;
using MM.Models;
using MM.Services.MAccount;
using MM.Services.MCommentService;
using MM.Services.MFood;
using MM.Services.MOrder;
using MM.Services.MOrderContent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.food
{
    public class FoodRoute : Root
    {
        private const string Default = Api + "/food";
        public const string Count = Default + "/count";
        public const string List = Default + "/list";
        public const string ListFavorite = Default + "/list-favorite";
        public const string ListRecently = Default + "/list-recently";
        public const string ListTopOrder = Default + "/list-top-order";
        public const string Get = Default + "/get";
        public const string Create = Default + "/create";
        public const string Update = Default + "/update";
        public const string Delete = Default + "/delete";
        public const string Vote = Default + "/vote";
        public const string Comment = Default + "/comment";
    }

    public class FoodController : ApiController
    {
        private DataContext DataContext;
        private IAccountService AccountService;
        private ICommentService CommentService;
        private IFoodService FoodService;
        private IOrderService OrderService;
        private IOrderContentService OrderContentService;
        private ICurrentContext CurrentContext;
        public FoodController(
            DataContext DataContext,
            IAccountService AccountService,
            ICommentService CommentService,
            IFoodService FoodService,
            IOrderService OrderService,
            IOrderContentService OrderContentService,
            ICurrentContext CurrentContext
        )
        {
            this.DataContext = DataContext;
            this.AccountService = AccountService;
            this.CommentService = CommentService;
            this.FoodService = FoodService;
            this.OrderService = OrderService;
            this.OrderContentService = OrderContentService;
            this.CurrentContext = CurrentContext;
        }

        [Route(FoodRoute.Count), HttpPost]
        public async Task<ActionResult<int>> Count([FromBody] Food_FoodFilterDTO Food_FoodFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            FoodFilter FoodFilter = ConvertFilterDTOToFilterEntity(Food_FoodFilterDTO);
            int count = await FoodService.Count(FoodFilter);
            return count;
        }

        [Route(FoodRoute.List), HttpPost]
        public async Task<ActionResult<List<Food_FoodDTO>>> List([FromBody] Food_FoodFilterDTO Food_FoodFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            FoodFilter FoodFilter = ConvertFilterDTOToFilterEntity(Food_FoodFilterDTO);
            if(FoodFilter.OrderBy == 0 && FoodFilter.OrderType == 0)
            {
                FoodFilter.OrderBy = FoodOrder.CreatedAt;
                FoodFilter.OrderType = OrderType.DESC;
            }
            List<Food> Foods = await FoodService.List(FoodFilter);
            List<Food_FoodDTO> Food_FoodDTOs = Foods
                .Select(c => new Food_FoodDTO(c)).ToList();
            return Food_FoodDTOs;
        }
        [Route(FoodRoute.ListFavorite), HttpPost]
        public async Task<ActionResult<List<Food_FoodDTO>>> ListFavorite([FromBody] Food_FoodFilterDTO Food_FoodFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);
            var query = from a in DataContext.Account
                        join aff in DataContext.AccountFoodFavorite on a.Id equals aff.AccountId
                        join f in DataContext.Food on aff.FoodId equals f.Id
                        join i in DataContext.Image on f.ImageId equals i.Id
                        where a.Id == CurrentContext.AccountId &&
                        f.StatusId == StatusEnum.ACTIVE.Id
                        orderby f.CreatedAt descending
                        select new Food_FoodDTO
                        {
                            Id = f.Id,
                            Name = f.Name,
                            PriceEach = f.PriceEach,
                            DiscountRate = f.DiscountRate,
                            Descreption = f.Descreption,
                            StatusId = f.StatusId,
                            ImageId = f.ImageId,
                            Image = f.Image == null ? null : new Food_ImageDTO
                            {
                                Id = f.Image.Id,
                                Url = f.Image.Url,
                            }
                        };

            List<Food_FoodDTO> Food_FoodDTOs = await query.Skip(Food_FoodFilterDTO.Skip).Take(Food_FoodFilterDTO.Take).ToListAsync();
            return Food_FoodDTOs.Distinct().ToList();
        }
        [Route(FoodRoute.ListRecently), HttpPost]
        public async Task<ActionResult<List<Food_FoodDTO>>> ListRecently([FromBody] Food_FoodFilterDTO Food_FoodFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);
            var query = from a in DataContext.Account
                        join o in DataContext.Order on a.Id equals o.AccountId
                        join oc in DataContext.OrderContent on o.Id equals oc.OrderId
                        join fftm in DataContext.FoodFoodTypeMapping on oc.FoodFoodTypeMappingId equals fftm.Id
                        join f in DataContext.Food on fftm.FoodId equals f.Id
                        join i in DataContext.Image on f.ImageId equals i.Id
                        where o.AccountId == CurrentContext.AccountId &&
                        o.StatusId == OrderStatusEnum.DONE.Id &&
                        f.StatusId == StatusEnum.ACTIVE.Id
                        orderby o.OrderDate descending
                        select new Food_FoodDTO
                        {
                            Id = f.Id,
                            Name = f.Name,
                            PriceEach = f.PriceEach,
                            DiscountRate = f.DiscountRate,
                            Descreption = f.Descreption,
                            StatusId = f.StatusId,
                            ImageId = f.ImageId,
                            Image = f.Image == null ? null : new Food_ImageDTO
                            {
                                Id = f.Image.Id,
                                Url = f.Image.Url,
                            }
                        };

            List<Food_FoodDTO> Food_FoodDTOs = await query.Skip(Food_FoodFilterDTO.Skip).Take(Food_FoodFilterDTO.Take).ToListAsync();
            return Food_FoodDTOs.Distinct().ToList();
        }
        [Route(FoodRoute.ListTopOrder), HttpPost]
        public async Task<ActionResult<List<Food_FoodDTO>>> ListTopOrder([FromBody] Food_FoodFilterDTO Food_FoodFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            var query = from o in DataContext.Order
                        join oc in DataContext.OrderContent on o.Id equals oc.OrderId
                        join fftm in DataContext.FoodFoodTypeMapping on oc.FoodFoodTypeMappingId equals fftm.Id
                        join f in DataContext.Food on fftm.FoodId equals f.Id
                        join i in DataContext.Image on f.ImageId equals i.Id
                        where o.StatusId == OrderStatusEnum.DONE.Id &&
                        f.StatusId == StatusEnum.ACTIVE.Id
                        orderby oc.Quantity descending
                        select new Food_FoodDTO
                        {
                            Id = f.Id,
                            Name = f.Name,
                            PriceEach = f.PriceEach,
                            DiscountRate = f.DiscountRate,
                            Descreption = f.Descreption,
                            StatusId = f.StatusId,
                            ImageId = f.ImageId,
                            Image = f.Image == null ? null : new Food_ImageDTO
                            {
                                Id = f.Image.Id,
                                Url = f.Image.Url,
                            }
                        };

            List<Food_FoodDTO> Food_FoodDTOs = await query.Skip(Food_FoodFilterDTO.Skip).Take(Food_FoodFilterDTO.Take).ToListAsync();
            return Food_FoodDTOs.Distinct().ToList();
        }

        [Route(FoodRoute.Get), HttpPost]
        public async Task<ActionResult<Food_FoodDTO>> Get([FromBody]Food_FoodDTO Food_FoodDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Food Food = await FoodService.Get(Food_FoodDTO.Id);
            return new Food_FoodDTO(Food);
        }

        [Route(FoodRoute.Create), HttpPost]
        public async Task<ActionResult<Food_FoodDTO>> Create([FromBody] Food_FoodDTO Food_FoodDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);
            
            Food Food = ConvertDTOToEntity(Food_FoodDTO);
            Food = await FoodService.Create(Food);
            Food_FoodDTO = new Food_FoodDTO(Food);
            if (Food.IsValidated)
                return Food_FoodDTO;
            else
                return BadRequest(Food_FoodDTO);
        }

        [Route(FoodRoute.Update), HttpPost]
        public async Task<ActionResult<Food_FoodDTO>> Update([FromBody] Food_FoodDTO Food_FoodDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);
            
            Food Food = ConvertDTOToEntity(Food_FoodDTO);
            Food = await FoodService.Update(Food);
            Food_FoodDTO = new Food_FoodDTO(Food);
            if (Food.IsValidated)
                return Food_FoodDTO;
            else
                return BadRequest(Food_FoodDTO);
        }

        [Route(FoodRoute.Delete), HttpPost]
        public async Task<ActionResult<Food_FoodDTO>> Delete([FromBody] Food_FoodDTO Food_FoodDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Food Food = ConvertDTOToEntity(Food_FoodDTO);
            Food = await FoodService.Delete(Food);
            Food_FoodDTO = new Food_FoodDTO(Food);
            if (Food.IsValidated)
                return Food_FoodDTO;
            else
                return BadRequest(Food_FoodDTO);
        }

        [Route(FoodRoute.Comment), HttpPost]
        public async Task<ActionResult<Food_CommentDTO>> Comment([FromBody] Food_CommentDTO Food_CommentDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Comment Comment = new Comment
            {
                Id = Food_CommentDTO.Id,
                Content = Food_CommentDTO.Content,
                FoodId = Food_CommentDTO.FoodId,
                AccountId = Food_CommentDTO.AccountId,
                Time = Food_CommentDTO.Time
            };
            Comment = await CommentService.Create(Comment);
            Food_CommentDTO = new Food_CommentDTO(Comment);
            if (Comment.IsValidated)
                return Food_CommentDTO;
            else
                return BadRequest(Food_CommentDTO);
        }

        [Route(FoodRoute.Vote), HttpPost]
        public async Task<ActionResult<Food_FoodDTO>> Vote([FromBody] Food_FoodAccountMappingDTO Food_FoodAccountMappingDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            FoodAccountMapping FoodAccountMapping = new FoodAccountMapping
            {
                FoodId = Food_FoodAccountMappingDTO.FoodId,
                AccountId = CurrentContext.AccountId,
                Rate = Food_FoodAccountMappingDTO.Rate,
            };
            Food Food = await FoodService.Vote(FoodAccountMapping);
            Food_FoodDTO Food_FoodDTO = new Food_FoodDTO(Food);
            if (Food.IsValidated)
                return Food_FoodDTO;
            else
                return BadRequest(Food_FoodDTO);
        }

        private Food ConvertDTOToEntity(Food_FoodDTO Food_FoodDTO)
        {
            Food Food = new Food();
            Food.Id = Food_FoodDTO.Id;
            Food.Name = Food_FoodDTO.Name;
            Food.PriceEach = Food_FoodDTO.PriceEach;
            Food.DiscountRate = Food_FoodDTO.DiscountRate;
            Food.ImageId = Food_FoodDTO.ImageId;
            Food.StatusId = Food_FoodDTO.StatusId;
            Food.Descreption = Food_FoodDTO.Descreption;
            Food.Rate = Food_FoodDTO.Rate;
            Food.Image = Food_FoodDTO.Image == null ? null : new Image
            {
                Id = Food_FoodDTO.Image.Id,
                Name = Food_FoodDTO.Image.Name,
                Path = Food_FoodDTO.Image.Path,
                Content = Food_FoodDTO.Image.Content,
                Url = Food_FoodDTO.Image.Url,
            };
            Food.FoodFoodGroupingMappings = Food_FoodDTO.FoodFoodGroupingMappings == null ? null : Food_FoodDTO.FoodFoodGroupingMappings.Select(x => new FoodFoodGroupingMapping
            {
                FoodId = x.FoodId,
                FoodGroupingId = x.FoodGroupingId,
            }).ToList();
            Food.FoodFoodTypeMappings = Food_FoodDTO.FoodFoodTypeMappings == null ? null : Food_FoodDTO.FoodFoodTypeMappings.Select(x => new FoodFoodTypeMapping
            {
                Id = x.Id,
                FoodId = x.FoodId,
                FoodTypeId = x.FoodTypeId,
            }).ToList();
            Food.BaseLanguage = CurrentContext.Language;
            return Food;
        }

        private FoodFilter ConvertFilterDTOToFilterEntity(Food_FoodFilterDTO Food_FoodFilterDTO)
        {
            FoodFilter FoodFilter = new FoodFilter();
            FoodFilter.Selects = FoodSelect.ALL;
            FoodFilter.Skip = Food_FoodFilterDTO.Skip;
            FoodFilter.Take = Food_FoodFilterDTO.Take;
            FoodFilter.OrderBy = Food_FoodFilterDTO.OrderBy;
            FoodFilter.OrderType = Food_FoodFilterDTO.OrderType;

            FoodFilter.Id = Food_FoodFilterDTO.Id;
            FoodFilter.Name = Food_FoodFilterDTO.Name;
            FoodFilter.PriceEach = Food_FoodFilterDTO.PriceEach;
            FoodFilter.DiscountRate = Food_FoodFilterDTO.DiscountRate;
            FoodFilter.StatusId = Food_FoodFilterDTO.StatusId;
            FoodFilter.Descreption = Food_FoodFilterDTO.Descreption;
            FoodFilter.FoodGroupingId = Food_FoodFilterDTO.FoodGroupingId;
            FoodFilter.Rate = Food_FoodFilterDTO.Rate;
            return FoodFilter;
        }


    }
}

