﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.food
{
    public class Food_FoodGroupingDTO : DataDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long StatusId { get; set; }

        public Food_FoodGroupingDTO() { }
        public Food_FoodGroupingDTO(FoodGrouping FoodGrouping) 
        {
            this.Id = FoodGrouping.Id;
            this.Name = FoodGrouping.Name;
            this.StatusId = FoodGrouping.StatusId;
            this.Errors = FoodGrouping.Errors;
        }
    }

    public class Food_FoodGroupingFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public StringFilter Name { get; set; }
        public IdFilter StatusId { get; set; }
        public FoodGroupingOrder OrderBy { get; set; }
    }
}
