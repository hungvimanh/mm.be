﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.food
{
    public class Food_FoodAccountMappingDTO : DataDTO
    {
        public long FoodId { get; set; }
        public long AccountId { get; set; }
        public long? Rate { get; set; }
        public Food_FoodAccountMappingDTO() { }
        public Food_FoodAccountMappingDTO(FoodAccountMapping FoodAccountMapping)
        {
            this.FoodId = FoodAccountMapping.FoodId;
            this.AccountId = FoodAccountMapping.AccountId;
            this.Rate = FoodAccountMapping.Rate;
        }
    }
}
