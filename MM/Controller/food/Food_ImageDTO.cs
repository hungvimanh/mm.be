﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.food
{
    public class Food_ImageDTO : DataDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
        public string MimeType { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }

        public Food_ImageDTO() { }
        public Food_ImageDTO(Image Image)
        {
            this.Id = Image.Id;
            this.Name = Image.Name;
            this.MimeType = Image.MimeType;
            this.Path = Image.Path;
            this.Url = Image.Url;
        }
    }
}
