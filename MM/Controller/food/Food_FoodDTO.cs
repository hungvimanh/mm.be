using Common;
using System;
using System.Linq;
using System.Collections.Generic;
using MM.Entities;
using MM.Enums;

namespace MM.Controller.food
{
    public class Food_FoodDTO : DataDTO, IEquatable<Food_FoodDTO>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal PriceEach { get; set; }
        public decimal? DiscountRate { get; set; }
        public long? ImageId { get; set; }
        public long StatusId { get; set; }
        public string Descreption { get; set; }
        public decimal Rate { get; set; }
        public long? MyRate { get; set; }
        public Food_ImageDTO Image { get; set; }
        public Food_StatusDTO Status
        {
            get
            {
                if (this.StatusId == FoodStatusEnum.ACTIVE.Id)
                    return new Food_StatusDTO
                    {
                        Id = FoodStatusEnum.ACTIVE.Id,
                        Code = FoodStatusEnum.ACTIVE.Code,
                        Name = FoodStatusEnum.ACTIVE.Name,
                    };
                if (this.StatusId == FoodStatusEnum.INACTIVE.Id)
                    return new Food_StatusDTO
                    {
                        Id = FoodStatusEnum.INACTIVE.Id,
                        Code = FoodStatusEnum.INACTIVE.Code,
                        Name = FoodStatusEnum.INACTIVE.Name,
                    };
                return null;
            }
        }
        public List<Food_CommentDTO> Comments { get; set; }
        public List<Food_FoodFoodTypeMappingDTO> FoodFoodTypeMappings { get; set; }
        public List<Food_FoodFoodGroupingMappingDTO> FoodFoodGroupingMappings { get; set; }
        public Food_FoodDTO() {}
        public Food_FoodDTO(Food Food)
        {
            this.Id = Food.Id;
            this.Name = Food.Name;
            this.PriceEach = Food.PriceEach;
            this.DiscountRate = Food.DiscountRate;
            this.ImageId = Food.ImageId;
            this.StatusId = Food.StatusId;
            this.Descreption = Food.Descreption;
            this.Rate = Food.Rate;
            this.MyRate = Food.MyRate;
            this.Image = Food.Image == null ? null : new Food_ImageDTO(Food.Image);
            this.Comments = Food.Comments?.Select(x => new Food_CommentDTO(x)).ToList();
            this.FoodFoodTypeMappings = Food.FoodFoodTypeMappings?.Select(x => new Food_FoodFoodTypeMappingDTO(x)).ToList();
            this.FoodFoodGroupingMappings = Food.FoodFoodGroupingMappings?.Select(x => new Food_FoodFoodGroupingMappingDTO(x)).ToList();
            this.Errors = Food.Errors;
        }
        public bool Equals(Food_FoodDTO other)
        {
            return other != null && Id == other.Id;
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }

    public class Food_FoodFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public StringFilter Name { get; set; }
        public DecimalFilter PriceEach { get; set; }
        public DecimalFilter DiscountRate { get; set; }
        public IdFilter StatusId { get; set; }
        public StringFilter Descreption { get; set; }
        public IdFilter FoodGroupingId { get; set; }
        public LongFilter Rate { get; set; }
        public FoodOrder OrderBy { get; set; }
    }
}
