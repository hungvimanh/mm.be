﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.food
{
    public class Food_FoodFoodGroupingMappingDTO : DataDTO
    {
        public long FoodId { get; set; }
        public long FoodGroupingId { get; set; }
        public Food_FoodGroupingDTO FoodGrouping { get; set; }
        public Food_FoodFoodGroupingMappingDTO() { }
        public Food_FoodFoodGroupingMappingDTO(FoodFoodGroupingMapping FoodFoodGroupingMapping) 
        {
            this.FoodId = FoodFoodGroupingMapping.FoodId;
            this.FoodGroupingId = FoodFoodGroupingMapping.FoodGroupingId;
            this.FoodGrouping = FoodFoodGroupingMapping.FoodGrouping == null ? null : new Food_FoodGroupingDTO(FoodFoodGroupingMapping.FoodGrouping);
            this.Errors = FoodFoodGroupingMapping.Errors;
        }
    }
}
