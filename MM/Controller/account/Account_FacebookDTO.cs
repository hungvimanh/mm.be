﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.account
{
    public class Account_FacebookDTO : DataDTO
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string age_range { get; set; }
        public string birthday { get; set; }
        public string gender { get; set; }
        public string locale { get; set; }
        public string picture { get; set; }
    }
}
