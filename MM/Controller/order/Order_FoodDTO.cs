﻿using Common;
using MM.Entities;
using MM.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.order
{
    public class Order_FoodDTO : DataDTO, IEquatable<Order_FoodDTO>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal PriceEach { get; set; }
        public decimal? DiscountRate { get; set; }
        public long? ImageId { get; set; }
        public long StatusId { get; set; }
        public string Descreption { get; set; }
        public Order_ImageDTO Image { get; set; }
        public Order_StatusDTO Status
        {
            get
            {
                if (this.StatusId == FoodStatusEnum.ACTIVE.Id)
                    return new Order_StatusDTO
                    {
                        Id = FoodStatusEnum.ACTIVE.Id,
                        Code = FoodStatusEnum.ACTIVE.Code,
                        Name = FoodStatusEnum.ACTIVE.Name,
                    };
                if (this.StatusId == FoodStatusEnum.INACTIVE.Id)
                    return new Order_StatusDTO
                    {
                        Id = FoodStatusEnum.INACTIVE.Id,
                        Code = FoodStatusEnum.INACTIVE.Code,
                        Name = FoodStatusEnum.INACTIVE.Name,
                    };
                return null;
            }
        }
        public Order_FoodDTO() { }
        public Order_FoodDTO(Food Food)
        {
            this.Id = Food.Id;
            this.Name = Food.Name;
            this.PriceEach = Food.PriceEach;
            this.DiscountRate = Food.DiscountRate;
            this.ImageId = Food.ImageId;
            this.StatusId = Food.StatusId;
            this.Descreption = Food.Descreption;
            this.Image = Food.Image == null ? null : new Order_ImageDTO(Food.Image);
            this.Errors = Food.Errors;
        }

        public bool Equals(Order_FoodDTO other)
        {
            return other != null && Id == other.Id;
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }

    public class Order_FoodFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public StringFilter Name { get; set; }
        public DecimalFilter PriceEach { get; set; }
        public DecimalFilter DiscountRate { get; set; }
        public StringFilter Image { get; set; }
        public IdFilter StatusId { get; set; }
        public StringFilter Descreption { get; set; }
        public IdFilter FoodGroupingId { get; set; }
        public FoodOrder OrderBy { get; set; }
    }
}
