﻿using Common;
using MM.Entities;
using MM.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.order
{
    public class Order_ReservationDTO : DataDTO
    {
        public long Id { get; set; }
        public long TableId { get; set; }
        public DateTime Date { get; set; }
        public long? OrderId { get; set; }
        public long StatusId { get; set; }
        public Order_TableDTO Table { get; set; }
        public Order_StatusDTO Status
        {
            get
            {
                if (this.StatusId == ReservationStatusEnum.BUSY.Id)
                    return new Order_StatusDTO
                    {
                        Id = ReservationStatusEnum.BUSY.Id,
                        Code = ReservationStatusEnum.BUSY.Code,
                        Name = ReservationStatusEnum.BUSY.Name,
                    };
                if (this.StatusId == ReservationStatusEnum.EMPTY.Id)
                    return new Order_StatusDTO
                    {
                        Id = ReservationStatusEnum.EMPTY.Id,
                        Code = ReservationStatusEnum.EMPTY.Code,
                        Name = ReservationStatusEnum.EMPTY.Name,
                    };
                return null;
            }
        }
        public Order_ReservationDTO() { }
        public Order_ReservationDTO(Reservation Reservation) 
        {
            this.Id = Reservation.Id;
            this.TableId = Reservation.TableId;
            this.Date = Reservation.Date;
            this.OrderId = Reservation.OrderId;
            this.StatusId = Reservation.StatusId;
            this.Table = Reservation.Table == null ? null : new Order_TableDTO(Reservation.Table);
            this.Errors = Reservation.Errors;
        }
    }

    public class Order_ReservationFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public IdFilter TableId { get; set; }
        public DateFilter Date { get; set; }
        public IdFilter OrderId { get; set; }
        public IdFilter StatusId { get; set; }
        public ReservationOrder OrderBy { get; set; }
    }
}
