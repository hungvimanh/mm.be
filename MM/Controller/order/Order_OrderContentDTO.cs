﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.order
{
    public class Order_OrderContentDTO : DataDTO
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public long OrderId { get; set; }
        public long FoodFoodTypeMappingId { get; set; }
        public long Quantity { get; set; }
        public long StatusId { get; set; }
        public decimal? Amount { get; set; }
        public Order_FoodFoodTypeMappingDTO FoodFoodTypeMapping { get; set; }
        public Order_OrderDTO Order { get; set; }
        public Order_OrderContentDTO() { }
        public Order_OrderContentDTO(OrderContent OrderContent)
        {
            this.Id = OrderContent.Id;
            this.Code = OrderContent.Code;
            this.OrderId = OrderContent.OrderId;
            this.FoodFoodTypeMappingId = OrderContent.FoodFoodTypeMappingId;
            this.Quantity = OrderContent.Quantity;
            this.StatusId = OrderContent.StatusId;
            this.Amount = OrderContent.Amount;
            this.FoodFoodTypeMapping = OrderContent.FoodFoodTypeMapping == null ? null : new Order_FoodFoodTypeMappingDTO(OrderContent.FoodFoodTypeMapping);
            this.Order = OrderContent.Order == null ? null : new Order_OrderDTO(OrderContent.Order);
            this.Errors = OrderContent.Errors;
        }
    }

    public class Order_OrderContentFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public StringFilter Code { get; set; }
        public IdFilter OrderId { get; set; }
        public IdFilter FoodFoodTypeMappingId { get; set; }
        public LongFilter Quantity { get; set; }
        public IdFilter StatusId { get; set; }
        public OrderContentOrder OrderBy { get; set; }
    }
}
