﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.order
{
    public class Order_TableDTO : DataDTO
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public long StatusId { get; set; }
        public Order_TableDTO() { }
        public Order_TableDTO(Table Table) 
        {
            this.Id = Table.Id;
            this.Code = Table.Code;
            this.StatusId = Table.StatusId;
            this.Errors = Table.Errors;
        }
    }

    public class Order_TableFilterDTO : FilterDTO 
    { 
        public IdFilter Id { get; set; }
        public StringFilter Code { get; set; }
        public IdFilter StatusId { get; set; }
        public OrderOrder OrderBy { get; set; }
    }
}
