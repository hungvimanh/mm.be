using Common;
using System;
using System.Linq;
using System.Collections.Generic;
using MM.Entities;
using MM.Enums;

namespace MM.Controller.order
{
    public class Order_OrderDTO : DataDTO
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? PayDate { get; set; }
        public long AccountId { get; set; }
        public long NumOfTable { get; set; }
        public long NumOfPerson { get; set; }
        public string Descreption { get; set; }
        public long StatusId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? Total { get; set; }
        public Order_AccountDTO Account { get; set; }
        public Order_StatusDTO Status { 
            get 
            {
                if (this.StatusId == OrderStatusEnum.NEW.Id)
                    return new Order_StatusDTO
                    {
                        Id = OrderStatusEnum.NEW.Id,
                        Code = OrderStatusEnum.NEW.Code,
                        Name = OrderStatusEnum.NEW.Name,
                    };
                if (this.StatusId == OrderStatusEnum.PENDING.Id)
                    return new Order_StatusDTO
                    {
                        Id = OrderStatusEnum.PENDING.Id,
                        Code = OrderStatusEnum.PENDING.Code,
                        Name = OrderStatusEnum.PENDING.Name,
                    };
                if (this.StatusId == OrderStatusEnum.APPROVED.Id)
                    return new Order_StatusDTO
                    {
                        Id = OrderStatusEnum.APPROVED.Id,
                        Code = OrderStatusEnum.APPROVED.Code,
                        Name = OrderStatusEnum.APPROVED.Name,
                    };
                if (this.StatusId == OrderStatusEnum.REJECTED.Id)
                    return new Order_StatusDTO
                    {
                        Id = OrderStatusEnum.REJECTED.Id,
                        Code = OrderStatusEnum.REJECTED.Code,
                        Name = OrderStatusEnum.REJECTED.Name,
                    };
                if (this.StatusId == OrderStatusEnum.DONE.Id)
                    return new Order_StatusDTO
                    {
                        Id = OrderStatusEnum.DONE.Id,
                        Code = OrderStatusEnum.DONE.Code,
                        Name = OrderStatusEnum.DONE.Name,
                    };
                return null;
            } 
        }
        public List<Order_OrderContentDTO> OrderContents { get; set; }
        public List<Order_ReservationDTO> Reservations { get; set; }
        public Order_OrderDTO() {}
        public Order_OrderDTO(Order Order)
        {
            this.Id = Order.Id;
            this.Code = Order.Code;
            this.OrderDate = Order.OrderDate;
            this.PayDate = Order.PayDate;
            this.AccountId = Order.AccountId;
            this.NumOfTable = Order.NumOfTable;
            this.NumOfPerson = Order.NumOfPerson;
            this.Descreption = Order.Descreption;
            this.StatusId = Order.StatusId;
            this.CreatedAt = Order.CreatedAt;
            this.UpdatedAt = Order.UpdatedAt;
            this.SubTotal = Order.SubTotal;
            this.Total = Order.Total;
            this.Account = Order.Account == null ? null : new Order_AccountDTO(Order.Account);
            this.OrderContents = Order.OrderContents?.Select(x => new Order_OrderContentDTO(x)).ToList();
            this.Reservations = Order.Reservations?.Select(x => new Order_ReservationDTO(x)).ToList();
            this.Errors = Order.Errors;
        }
    }

    public class Order_OrderFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public StringFilter Code { get; set; }
        public DateFilter OrderDate { get; set; }
        public DateFilter PayDate { get; set; }
        public IdFilter AccountId { get; set; }
        public LongFilter NumOfTable { get; set; }
        public LongFilter NumOfPerson { get; set; }
        public StringFilter Descreption { get; set; }
        public IdFilter StatusId { get; set; }
        public DateFilter CreatedAt { get; set; }
        public OrderOrder OrderBy { get; set; }
    }
}
