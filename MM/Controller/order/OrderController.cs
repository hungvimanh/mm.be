using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MM.Entities;
using MM.Enums;
using MM.Models;
using MM.Services.MFood;
using MM.Services.MOrder;
using MM.Services.MOrderContent;
using MM.Services.MReservation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.order
{
    public class OrderRoute : Root
    {
        private const string Default = Api + "/order";
        public const string Count = Default + "/count";
        public const string List = Default + "/list";
        public const string Get = Default + "/get";
        public const string Create = Default + "/create";
        public const string Update = Default + "/update";
        public const string Approve = Default + "/approve";
        public const string Reject = Default + "/reject";
        public const string Done = Default + "/done";
        public const string Delete = Default + "/delete";
        public const string CountReservation = Default + "/count-reservation";
        public const string ListReservation = Default + "/list-reservation";

        public const string ListHistory = Default + "/list-history";
        public const string ListFood = Default + "/list-food";
        public const string ListRecently = Default + "/list-food-recently";
        public const string ListTopOrder = Default + "/list-food-top-order";
    }

    public class OrderController : ApiController
    {
        private DataContext DataContext;
        private IOrderService OrderService;
        private IOrderContentService OrderContentService;
        private IReservationService ReservationService;
        private IFoodService FoodService;
        private ICurrentContext CurrentContext;
        public OrderController(
            DataContext DataContext,
            IOrderService OrderService,
            IOrderContentService OrderContentService,
            IReservationService ReservationService,
            IFoodService FoodService,
            ICurrentContext CurrentContext
        )
        {
            this.DataContext = DataContext;
            this.OrderService = OrderService;
            this.OrderContentService = OrderContentService;
            this.ReservationService = ReservationService;
            this.FoodService = FoodService;
            this.CurrentContext = CurrentContext;
        }

        [Route(OrderRoute.Count), HttpPost]
        public async Task<ActionResult<int>> Count([FromBody] Order_OrderFilterDTO Order_OrderFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            OrderFilter OrderFilter = ConvertFilterDTOToFilterEntity(Order_OrderFilterDTO);
            int count = await OrderService.Count(OrderFilter);
            return count;
        }

        [Route(OrderRoute.List), HttpPost]
        public async Task<ActionResult<List<Order_OrderDTO>>> List([FromBody] Order_OrderFilterDTO Order_OrderFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            OrderFilter OrderFilter = ConvertFilterDTOToFilterEntity(Order_OrderFilterDTO);
            List<Order> Orders = await OrderService.List(OrderFilter);
            List<Order_OrderDTO> Order_OrderDTOs = Orders
                .Select(c => new Order_OrderDTO(c)).ToList();
            return Order_OrderDTOs;
        }

        [Route(OrderRoute.ListHistory), HttpPost]
        public async Task<ActionResult<List<Order_OrderDTO>>> ListHistory([FromBody] Order_OrderFilterDTO Order_OrderFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            OrderFilter OrderFilter = ConvertFilterDTOToFilterEntity(Order_OrderFilterDTO);
            OrderFilter.AccountId = new IdFilter { Equal = CurrentContext.AccountId };
            List<Order> Orders = await OrderService.List(OrderFilter);
            List<Order_OrderDTO> Order_OrderDTOs = Orders
                .Select(c => new Order_OrderDTO(c)).ToList();
            return Order_OrderDTOs;
        }

        [Route(OrderRoute.Get), HttpPost]
        public async Task<ActionResult<Order_OrderDTO>> Get([FromBody]Order_OrderDTO Order_OrderDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Order Order = await OrderService.Get(Order_OrderDTO.Id);
            return new Order_OrderDTO(Order);
        }

        [Route(OrderRoute.Create), HttpPost]
        public async Task<ActionResult<Order_OrderDTO>> Create([FromBody] Order_OrderDTO Order_OrderDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);
            
            Order Order = ConvertDTOToEntity(Order_OrderDTO);
            Order.AccountId = ExtractUserId();
            Order = await OrderService.Create(Order);
            Order_OrderDTO = new Order_OrderDTO(Order);
            if (Order.IsValidated)
                return Order_OrderDTO;
            else
                return BadRequest(Order_OrderDTO);
        }

        [Route(OrderRoute.Update), HttpPost]
        public async Task<ActionResult<Order_OrderDTO>> Update([FromBody] Order_OrderDTO Order_OrderDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);
            
            Order Order = ConvertDTOToEntity(Order_OrderDTO);
            Order = await OrderService.Update(Order);
            Order_OrderDTO = new Order_OrderDTO(Order);
            if (Order.IsValidated)
                return Order_OrderDTO;
            else
                return BadRequest(Order_OrderDTO);
        }

        [Route(OrderRoute.Approve), HttpPost]
        public async Task<ActionResult<Order_OrderDTO>> Approve([FromBody] Order_OrderDTO Order_OrderDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Order Order = ConvertDTOToEntity(Order_OrderDTO);
            Order = await OrderService.Approve(Order);
            Order_OrderDTO = new Order_OrderDTO(Order);
            if (Order.IsValidated)
                return Order_OrderDTO;
            else
                return BadRequest(Order_OrderDTO);
        }

        [Route(OrderRoute.Reject), HttpPost]
        public async Task<ActionResult<Order_OrderDTO>> Reject([FromBody] Order_OrderDTO Order_OrderDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Order Order = ConvertDTOToEntity(Order_OrderDTO);
            Order = await OrderService.Reject(Order);
            Order_OrderDTO = new Order_OrderDTO(Order);
            if (Order.IsValidated)
                return Order_OrderDTO;
            else
                return BadRequest(Order_OrderDTO);
        }

        [Route(OrderRoute.Done), HttpPost]
        public async Task<ActionResult<Order_OrderDTO>> Done([FromBody] Order_OrderDTO Order_OrderDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Order Order = ConvertDTOToEntity(Order_OrderDTO);
            Order = await OrderService.Done(Order);
            Order_OrderDTO = new Order_OrderDTO(Order);
            if (Order.IsValidated)
                return Order_OrderDTO;
            else
                return BadRequest(Order_OrderDTO);
        }

        [Route(OrderRoute.Delete), HttpPost]
        public async Task<ActionResult<Order_OrderDTO>> Delete([FromBody] Order_OrderDTO Order_OrderDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Order Order = ConvertDTOToEntity(Order_OrderDTO);
            Order = await OrderService.Delete(Order);
            Order_OrderDTO = new Order_OrderDTO(Order);
            if (Order.IsValidated)
                return Order_OrderDTO;
            else
                return BadRequest(Order_OrderDTO);
        }
        
        private Order ConvertDTOToEntity(Order_OrderDTO Order_OrderDTO)
        {
            Order Order = new Order();
            Order.Id = Order_OrderDTO.Id;
            Order.Code = Order_OrderDTO.Code;
            Order.OrderDate = Order_OrderDTO.OrderDate;
            Order.PayDate = Order_OrderDTO.PayDate;
            Order.AccountId = Order_OrderDTO.AccountId;
            Order.NumOfTable = Order_OrderDTO.NumOfTable;
            Order.NumOfPerson = Order_OrderDTO.NumOfPerson;
            Order.Descreption = Order_OrderDTO.Descreption;
            Order.SubTotal = Order_OrderDTO.SubTotal;
            Order.Total = Order_OrderDTO.Total;
            Order.StatusId = Order_OrderDTO.StatusId;
            Order.Account = Order_OrderDTO.Account == null ? null : new Account
            {
                Id = Order_OrderDTO.Account.Id,
                DisplayName = Order_OrderDTO.Account.DisplayName,
                Email = Order_OrderDTO.Account.Email,
                Phone = Order_OrderDTO.Account.Phone,
                Password = Order_OrderDTO.Account.Password,
                Salt = Order_OrderDTO.Account.Salt,
                PasswordRecoveryCode = Order_OrderDTO.Account.PasswordRecoveryCode,
                ExpiredTimeCode = Order_OrderDTO.Account.ExpiredTimeCode,
                Address = Order_OrderDTO.Account.Address,
                Dob = Order_OrderDTO.Account.Dob,
                ImageId = Order_OrderDTO.Account.ImageId,
                RoleId = Order_OrderDTO.Account.RoleId,
            };
            Order.OrderContents = Order_OrderDTO.OrderContents == null ? null : Order_OrderDTO.OrderContents.Select(x => new OrderContent
            {
                Id = x.Id,
                Code = x.Code,
                FoodFoodTypeMappingId = x.FoodFoodTypeMappingId,
                OrderId = x.OrderId,
                Quantity = x.Quantity,
                Amount = x.Amount,
                StatusId = x.StatusId,
                FoodFoodTypeMapping = x.FoodFoodTypeMapping == null ? null : new FoodFoodTypeMapping
                {
                    Id = x.FoodFoodTypeMapping.Id,
                    FoodId = x.FoodFoodTypeMapping.FoodId,
                    FoodTypeId = x.FoodFoodTypeMapping.FoodTypeId
                }
            }).ToList();
            Order.Reservations = Order_OrderDTO.Reservations?.Select(x => new Reservation
            {
                Id = x.Id,
                Date = x.Date,
                StatusId = x.StatusId,
                OrderId = x.OrderId,
                TableId = x.TableId,
                Table = x.Table == null ? null : new Table
                {
                    Id = x.Table.Id,
                    Code = x.Table.Code,
                    StatusId = x.Table.StatusId,
                }
            }).ToList();
            Order.BaseLanguage = CurrentContext.Language;
            return Order;
        }

        private OrderFilter ConvertFilterDTOToFilterEntity(Order_OrderFilterDTO Order_OrderFilterDTO)
        {
            OrderFilter OrderFilter = new OrderFilter();
            OrderFilter.Selects = OrderSelect.ALL;
            OrderFilter.Skip = Order_OrderFilterDTO.Skip;
            OrderFilter.Take = Order_OrderFilterDTO.Take;
            OrderFilter.OrderBy = Order_OrderFilterDTO.OrderBy;
            OrderFilter.OrderType = Order_OrderFilterDTO.OrderType;

            OrderFilter.Id = Order_OrderFilterDTO.Id;
            OrderFilter.Code = Order_OrderFilterDTO.Code;
            OrderFilter.OrderDate = Order_OrderFilterDTO.OrderDate;
            OrderFilter.PayDate = Order_OrderFilterDTO.PayDate;
            OrderFilter.AccountId = Order_OrderFilterDTO.AccountId;
            OrderFilter.NumOfTable = Order_OrderFilterDTO.NumOfTable;
            OrderFilter.NumOfPerson = Order_OrderFilterDTO.NumOfPerson;
            OrderFilter.Descreption = Order_OrderFilterDTO.Descreption;
            OrderFilter.CreatedAt = Order_OrderFilterDTO.CreatedAt;
            OrderFilter.StatusId = Order_OrderFilterDTO.StatusId;
            return OrderFilter;
        }

        [Route(OrderRoute.CountReservation), HttpPost]
        public async Task<int> CountReservation([FromBody] Order_ReservationFilterDTO Order_ReservationFilterDTO)
        {
            if (Order_ReservationFilterDTO.Date == null)
                Order_ReservationFilterDTO.Date = new DateFilter { Equal = DateTime.Now.Date.AddHours(7) };
            else if(Order_ReservationFilterDTO.Date != null && !Order_ReservationFilterDTO.Date.Equal.HasValue)
                Order_ReservationFilterDTO.Date = new DateFilter { Equal = DateTime.Now.Date.AddHours(7) };
            ReservationFilter ReservationFilter = new ReservationFilter
            {
                Date = Order_ReservationFilterDTO.Date,
                StatusId = new IdFilter { Equal = ReservationStatusEnum.EMPTY.Id }
            };

            var count = await ReservationService.Count(ReservationFilter);
            return count;
        }

        [Route(OrderRoute.ListReservation), HttpPost]
        public async Task<List<Order_ReservationDTO>> ListReservation([FromBody] Order_ReservationFilterDTO Order_ReservationFilterDTO)
        {
            ReservationFilter ReservationFilter = new ReservationFilter
            {
                Skip = Order_ReservationFilterDTO.Skip,
                Take = Order_ReservationFilterDTO.Take,
                OrderBy = ReservationOrder.Id,
                OrderType = OrderType.ASC,
                Selects = ReservationSelect.ALL,

                Id = Order_ReservationFilterDTO.Id,
                Date = Order_ReservationFilterDTO.Date,
                StatusId = new IdFilter { Equal = ReservationStatusEnum.EMPTY.Id }
            };
            if (Order_ReservationFilterDTO.Date == null)
                Order_ReservationFilterDTO.Date = new DateFilter { Equal = DateTime.Now.Date.AddHours(7) };
            else if (Order_ReservationFilterDTO.Date != null && !Order_ReservationFilterDTO.Date.Equal.HasValue)
                Order_ReservationFilterDTO.Date = new DateFilter { Equal = DateTime.Now.Date.AddHours(7) };
            var Reservations = await ReservationService.List(ReservationFilter);
            var ReservationDTOs = Reservations.Select(x => new Order_ReservationDTO(x)).ToList();
            return ReservationDTOs;
        }

        [Route(OrderRoute.ListFood), HttpPost]
        public async Task<List<Order_FoodDTO>> ListFood([FromBody] Order_FoodFilterDTO Order_FoodFilterDTO)
        {
            FoodFilter FoodFilter = new FoodFilter
            {
                Skip = Order_FoodFilterDTO.Skip,
                Take = Order_FoodFilterDTO.Take,
                OrderBy = Order_FoodFilterDTO.OrderBy,
                OrderType = Order_FoodFilterDTO.OrderType,
                Selects = FoodSelect.ALL,

                Id = Order_FoodFilterDTO.Id,
                Name = Order_FoodFilterDTO.Name,
                PriceEach = Order_FoodFilterDTO.PriceEach,
                DiscountRate = Order_FoodFilterDTO.DiscountRate,
                FoodGroupingId = Order_FoodFilterDTO.FoodGroupingId,
                StatusId = new IdFilter { Equal = FoodStatusEnum.ACTIVE.Id }
            };
            if (FoodFilter.OrderBy == 0 && FoodFilter.OrderType == 0)
            {
                FoodFilter.OrderBy = FoodOrder.CreatedAt;
                FoodFilter.OrderType = OrderType.DESC;
            }
            var Foods = await FoodService.List(FoodFilter);
            List<Order_FoodDTO> Order_FoodDTOs = Foods.Select(x => new Order_FoodDTO(x)).ToList();
            return Order_FoodDTOs;
        }

        [Route(OrderRoute.ListRecently), HttpPost]
        public async Task<ActionResult<List<Order_FoodDTO>>> ListRecently([FromBody] Order_FoodFilterDTO Order_FoodFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            var query = from a in DataContext.Account
                        join o in DataContext.Order on a.Id equals o.AccountId
                        join oc in DataContext.OrderContent on o.Id equals oc.OrderId
                        join fftm in DataContext.FoodFoodTypeMapping on oc.FoodFoodTypeMappingId equals fftm.Id
                        join f in DataContext.Food on fftm.FoodId equals f.Id
                        join i in DataContext.Image on f.ImageId equals i.Id
                        where o.AccountId == CurrentContext.AccountId &&
                        o.StatusId == OrderStatusEnum.DONE.Id &&
                        f.StatusId == StatusEnum.ACTIVE.Id
                        orderby o.OrderDate descending
                        select new Order_FoodDTO
                        {
                            Id = f.Id,
                            Name = f.Name,
                            PriceEach = f.PriceEach,
                            DiscountRate = f.DiscountRate,
                            Descreption = f.Descreption,
                            StatusId = f.StatusId,
                            ImageId = f.ImageId,
                            Image = f.Image == null ? null : new Order_ImageDTO
                            {
                                Id = f.Image.Id,
                                Url = f.Image.Url,
                            }
                        };

            List<Order_FoodDTO> Order_FoodDTOs = await query.Skip(Order_FoodFilterDTO.Skip).Take(Order_FoodFilterDTO.Take).ToListAsync();
            return Order_FoodDTOs.Distinct().ToList();
        }
        [Route(OrderRoute.ListTopOrder), HttpPost]
        public async Task<ActionResult<List<Order_FoodDTO>>> ListTopOrder([FromBody] Order_FoodFilterDTO Order_FoodFilterDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            var query = from o in DataContext.Order
                        join oc in DataContext.OrderContent on o.Id equals oc.OrderId
                        join fftm in DataContext.FoodFoodTypeMapping on oc.FoodFoodTypeMappingId equals fftm.Id
                        join f in DataContext.Food on fftm.FoodId equals f.Id
                        join i in DataContext.Image on f.ImageId equals i.Id
                        where o.StatusId == OrderStatusEnum.DONE.Id &&
                        f.StatusId == StatusEnum.ACTIVE.Id
                        orderby oc.Quantity descending
                        select new Order_FoodDTO
                        {
                            Id = f.Id,
                            Name = f.Name,
                            PriceEach = f.PriceEach,
                            DiscountRate = f.DiscountRate,
                            Descreption = f.Descreption,
                            StatusId = f.StatusId,
                            ImageId = f.ImageId,
                            Image = f.Image == null ? null : new Order_ImageDTO
                            {
                                Id = f.Image.Id,
                                Url = f.Image.Url,
                            }
                        };

            List<Order_FoodDTO> Order_FoodDTOs = await query.Skip(Order_FoodFilterDTO.Skip).Take(Order_FoodFilterDTO.Take).ToListAsync();
            return Order_FoodDTOs.Distinct().ToList();
        }
    }
}

