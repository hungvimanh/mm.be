﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.order
{
    public class Order_ImageDTO : DataDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
        public string MimeType { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }

        public Order_ImageDTO() { }
        public Order_ImageDTO(Image Image) 
        {
            this.Id = Image.Id;
            this.Name = Image.Name;
            this.MimeType = Image.MimeType;
            this.Path = Image.Path;
            this.Url = Image.Url;
        }
    }
}
