﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.order
{
    public class Order_FoodTypeDTO : DataDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long StatusId { get; set; }
        public Order_FoodTypeDTO() { }
        public Order_FoodTypeDTO(FoodType FoodType)
        {
            this.Id = FoodType.Id;
            this.Name = FoodType.Name;
            this.StatusId = FoodType.StatusId;
            this.Errors = FoodType.Errors;
        }
    }

    public class Order_FoodTypeFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public StringFilter Name { get; set; }
        public IdFilter StatusId { get; set; }
        public FoodTypeOrder OrderBy { get; set; }
    }
}
