﻿using Common;
using MM.Entities;
using MM.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.report
{
    public class Report_ReservationDTO : DataDTO
    {
        public long Id { get; set; }
        public long TableId { get; set; }
        public DateTime Date { get; set; }
        public long? OrderId { get; set; }
        public long StatusId { get; set; }
        public Report_TableDTO Table { get; set; }
        public Report_StatusDTO Status
        {
            get
            {
                if (this.StatusId == ReservationStatusEnum.BUSY.Id)
                    return new Report_StatusDTO
                    {
                        Id = ReservationStatusEnum.BUSY.Id,
                        Code = ReservationStatusEnum.BUSY.Code,
                        Name = ReservationStatusEnum.BUSY.Name,
                    };
                if (this.StatusId == ReservationStatusEnum.EMPTY.Id)
                    return new Report_StatusDTO
                    {
                        Id = ReservationStatusEnum.EMPTY.Id,
                        Code = ReservationStatusEnum.EMPTY.Code,
                        Name = ReservationStatusEnum.EMPTY.Name,
                    };
                return null;
            }
        }
        public Report_ReservationDTO() { }
        public Report_ReservationDTO(Reservation Reservation)
        {
            this.Id = Reservation.Id;
            this.TableId = Reservation.TableId;
            this.Date = Reservation.Date;
            this.OrderId = Reservation.OrderId;
            this.StatusId = Reservation.StatusId;
            this.Table = Reservation.Table == null ? null : new Report_TableDTO(Reservation.Table);
            this.Errors = Reservation.Errors;
        }
    }

    public class Report_ReservationFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public IdFilter TableId { get; set; }
        public DateFilter Date { get; set; }
        public IdFilter OrderId { get; set; }
        public IdFilter StatusId { get; set; }
        public ReservationOrder OrderBy { get; set; }
    }
}
