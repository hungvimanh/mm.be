﻿using Common;
using Microsoft.AspNetCore.Mvc;
using MM.Models;
using MM.Services.MOrder;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.report
{
    public class ReportRoute : Root
    {
        public const string Default = Api + "/report";
        public const string Get = Default + "/get";
    }
    public class ReportController : ApiController
    {
        private IOrderService OrderService;
        public ReportController(IOrderService OrderService)
        {
            this.OrderService = OrderService;
        }

        [Route(ReportRoute.Default), HttpPost]
        public async Task<Report_ReportDTO> Report([FromBody] Report_ReportFilterDTO Report_ReportFilterDTO)
        {
            DateTime StartDate = new DateTime();
            DateTime EndDate = new DateTime();
            List<Order> Orders = new List<Order>();
            Report_ReportDTO Report_ReportDTO = new Report_ReportDTO();
            if (Report_ReportFilterDTO.TypeId.Equal.HasValue)
            {
                int currentMonth = DateTime.Now.Month;
                int currentYear = DateTime.Now.Year;
                if (Report_ReportFilterDTO.TypeId.Equal.Value == Enums.ReportEnum.MONTH.Id)
                {
                    StartDate = new DateTime(currentYear, currentMonth, 1);
                    EndDate = StartDate.AddMonths(1).AddDays(-1);

                    Report_ReportDTO.Start = StartDate;
                    Report_ReportDTO.End = EndDate;

                    OrderFilter OrderFilter = new OrderFilter
                    {
                        Skip = Report_ReportFilterDTO.Skip,
                        Take = Report_ReportFilterDTO.Take,
                        OrderBy = OrderOrder.OrderDate,
                        OrderType = OrderType.DESC,
                        OrderDate = new DateFilter { GreaterEqual = StartDate, LessEqual = EndDate },
                        Selects = OrderSelect.ALL,
                        StatusId = new IdFilter { Equal = Enums.OrderStatusEnum.DONE.Id }
                    };

                    Orders = await OrderService.List(OrderFilter);
                }

                if (Report_ReportFilterDTO.TypeId.Equal.Value == Enums.ReportEnum.YEAR.Id)
                {
                    StartDate = new DateTime(currentYear, 1, 1);
                    EndDate = StartDate.AddYears(1).AddDays(-1);

                    Report_ReportDTO.Start = StartDate;
                    Report_ReportDTO.End = EndDate;

                    OrderFilter OrderFilter = new OrderFilter
                    {
                        Skip = Report_ReportFilterDTO.Skip,
                        Take = Report_ReportFilterDTO.Take,
                        OrderBy = OrderOrder.OrderDate,
                        OrderType = OrderType.DESC,
                        OrderDate = new DateFilter { GreaterEqual = StartDate, LessEqual = EndDate },
                        Selects = OrderSelect.ALL,
                        StatusId = new IdFilter { Equal = Enums.OrderStatusEnum.DONE.Id }
                    };

                    Orders = await OrderService.List(OrderFilter);
                }
            }

            if(Orders != null && Orders.Any())
            {
                Report_ReportDTO.Orders = Orders?.Select(x => new Report_OrderDTO(x)).ToList();
                return Report_ReportDTO;
            }
            return null;
        }

        [Route(ReportRoute.Get), HttpPost]
        public async Task<ActionResult<Report_OrderDTO>> Get([FromBody] Report_OrderDTO Report_OrderDTO)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);

            Order Order = await OrderService.Get(Report_OrderDTO.Id);
            return new Report_OrderDTO(Order);
        }
    }
}
