﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.report
{
    public class Report_FoodDTO : DataDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal PriceEach { get; set; }
        public decimal? DiscountRate { get; set; }
        public long StatusId { get; set; }
        public string Descreption { get; set; }
        
        public Report_FoodDTO() { }
        public Report_FoodDTO(Food Food)
        {
            this.Id = Food.Id;
            this.Name = Food.Name;
            this.PriceEach = Food.PriceEach;
            this.DiscountRate = Food.DiscountRate;
            this.StatusId = Food.StatusId;
            this.Descreption = Food.Descreption;
            this.Errors = Food.Errors;
        }
    }

    public class Report_FoodFilterDTO : FilterDTO
    {
        public IdFilter Id { get; set; }
        public StringFilter Name { get; set; }
        public DecimalFilter PriceEach { get; set; }
        public DecimalFilter DiscountRate { get; set; }
        public StringFilter Image { get; set; }
        public IdFilter StatusId { get; set; }
        public StringFilter Descreption { get; set; }
        public IdFilter FoodGroupingId { get; set; }
        public FoodOrder OrderBy { get; set; }
    }
}
