﻿using Common;
using MM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.report
{
    public class Report_ReportDTO : DataDTO
    {
        public int Count => Orders.Count();
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public List<Report_OrderDTO> Orders { get; set; }
    }

    public class Report_ReportFilterDTO : FilterDTO 
    {
        public IdFilter TypeId { get; set; }
    }
}
