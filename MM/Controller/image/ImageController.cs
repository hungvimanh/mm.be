﻿using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MM.Entities;
using MM.Services.MImage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Controller.image
{
    public class ImageRoute : Root
    {
        public const string Upload = Api + "/image/upload";
        public const string DownloadPath01 = Api + "/image/download/{path01}";
        public const string DownloadPath02 = Api + "/image/download/{path01}/{path02}";
        public const string DownloadPath03 = Api + "/image/download/{path01}/{path02}/{path03}";
        public const string DownloadPath04 = Api + "/image/download/{path01}/{path02}/{path03}/{path04}";
        public const string DownloadPath05 = Api + "/image/download/{path01}/{path02}/{path03}/{path04}/{path05}";
        public const string DownloadPath06 = Api + "/image/download/{path01}/{path02}/{path03}/{path04}/{path05}/{path06}";
        public const string DownloadPath07 = Api + "/image/download/{path01}/{path02}/{path03}/{path04}/{path05}/{path06}/{path07}";
        public const string DownloadPath08 = Api + "/image/download/{path01}/{path02}/{path03}/{path04}/{path05}/{path06}/{path07}/{path08}";
        public const string DownloadPath09 = Api + "/image/download/{path01}/{path02}/{path03}/{path04}/{path05}/{path06}/{path07}/{path08}/{path09}";
        public const string DownloadPath10 = Api + "/image/download/{path01}/{path02}/{path03}/{path04}/{path05}/{path06}/{path07}/{path08}/{path09}/{path10}";
    }
    [ApiController]
    public class ImageController : ControllerBase
    {
        private IImageService ImageService;
        public ImageController(IImageService ImageService)
        {
            this.ImageService = ImageService;
        }

        [HttpPost, HttpGet]
        [Route(ImageRoute.DownloadPath01)]
        [Route(ImageRoute.DownloadPath02)]
        [Route(ImageRoute.DownloadPath03)]
        [Route(ImageRoute.DownloadPath04)]
        [Route(ImageRoute.DownloadPath05)]
        [Route(ImageRoute.DownloadPath06)]
        [Route(ImageRoute.DownloadPath07)]
        [Route(ImageRoute.DownloadPath08)]
        [Route(ImageRoute.DownloadPath09)]
        [Route(ImageRoute.DownloadPath10)]
        public async Task<ActionResult> Download(string path01, string path02, string path03, string path04, string path05,
            string path06, string path07, string path08, string path09, string path10)
        {
            List<string> paths = new List<string>();
            paths.Add("");
            if (!string.IsNullOrWhiteSpace(path01)) paths.Add(path01.ToLower());
            if (!string.IsNullOrWhiteSpace(path02)) paths.Add(path02.ToLower());
            if (!string.IsNullOrWhiteSpace(path03)) paths.Add(path03.ToLower());
            if (!string.IsNullOrWhiteSpace(path04)) paths.Add(path04.ToLower());
            if (!string.IsNullOrWhiteSpace(path05)) paths.Add(path05.ToLower());
            if (!string.IsNullOrWhiteSpace(path06)) paths.Add(path06.ToLower());
            if (!string.IsNullOrWhiteSpace(path07)) paths.Add(path07.ToLower());
            if (!string.IsNullOrWhiteSpace(path08)) paths.Add(path08.ToLower());
            if (!string.IsNullOrWhiteSpace(path09)) paths.Add(path09.ToLower());
            if (!string.IsNullOrWhiteSpace(path10)) paths.Add(path10.ToLower());
            string path = string.Join("/", paths);
            ImageFilter fileFilter = new ImageFilter
            {
                Path = new StringFilter { Equal = path },
                Skip = 0,
                Take = 1,
                Selects = ImageSelect.ALL
            };
            var files = await ImageService.List(fileFilter);
            Entities.Image file = files.FirstOrDefault();
            if (file == null)
                return BadRequest();
            file = await ImageService.Download(file.Id);
            return File(file.Content, file.MimeType, file.Name);
        }

        [HttpPost]
        [Route(ImageRoute.Upload)]
        public async Task<Entities.Image> Upload(IFormFile file)
        {
            if (!ModelState.IsValid)
                throw new BindException(ModelState);
            MemoryStream memoryStream = new MemoryStream();
            file.CopyTo(memoryStream);
            Image Image = new Image
            {
                Name = file.FileName,
                Content = memoryStream.ToArray()
            };
            Image = await ImageService.Upload(Image);
            if (Image == null)
                return null;
            return Image;
        }
    }
}
