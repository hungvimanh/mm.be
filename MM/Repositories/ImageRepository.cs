﻿using MM.Entities;
using MM.Models;
using System;
using Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MM.Repositories
{
    public interface IImageRepository
    {
        Task<int> Count(ImageFilter ImageFilter);
        Task<List<Image>> List(ImageFilter ImageFilter);
        Task<Image> Get(long Id);
        Task<bool> Create(Image Image);
    }
    public class ImageRepository : IImageRepository
    {
        private DataContext DataContext;
        public ImageRepository(DataContext DataContext)
        {
            this.DataContext = DataContext;
        }

        private IQueryable<ImageDAO> DynamicFilter(IQueryable<ImageDAO> query, ImageFilter filter)
        {
            if (filter == null)
                return query.Where(q => false);
            if (filter.Id != null)
                query = query.Where(q => q.Id, filter.Id);
            if (filter.Name != null)
                query = query.Where(q => q.Name, filter.Name);
            if (filter.Url != null)
                query = query.Where(q => q.Url, filter.Url);
            if (filter.Path != null)
                query = query.Where(q => q.Path, filter.Path);
            return query;
        }

        private IQueryable<ImageDAO> DynamicOrder(IQueryable<ImageDAO> query, ImageFilter filter)
        {
            switch (filter.OrderType)
            {
                case OrderType.ASC:
                    switch (filter.OrderBy)
                    {
                        case ImageOrder.Id:
                            query = query.OrderBy(q => q.Id);
                            break;
                        case ImageOrder.Name:
                            query = query.OrderBy(q => q.Name);
                            break;
                        case ImageOrder.Url:
                            query = query.OrderBy(q => q.Url);
                            break;
                        case ImageOrder.Path:
                            query = query.OrderBy(q => q.Path);
                            break;
                    }
                    break;
                case OrderType.DESC:
                    switch (filter.OrderBy)
                    {
                        case ImageOrder.Id:
                            query = query.OrderByDescending(q => q.Id);
                            break;
                        case ImageOrder.Name:
                            query = query.OrderByDescending(q => q.Name);
                            break;
                        case ImageOrder.Url:
                            query = query.OrderByDescending(q => q.Url);
                            break;
                        case ImageOrder.Path:
                            query = query.OrderByDescending(q => q.Path);
                            break;
                    }
                    break;
            }
            query = query.Skip(filter.Skip).Take(filter.Take);
            return query;
        }

        private async Task<List<Image>> DynamicSelect(IQueryable<ImageDAO> query, ImageFilter filter)
        {
            List<Image> Images = await query.Select(q => new Image()
            {
                Id = filter.Selects.Contains(ImageSelect.Id) ? q.Id : default(long),
                Name = filter.Selects.Contains(ImageSelect.Name) ? q.Name : default(string),
                Url = filter.Selects.Contains(ImageSelect.Url) ? q.Url : default(string),
                Path = filter.Selects.Contains(ImageSelect.Path) ? q.Path : default(string),
            }).ToListAsync();
            return Images;
        }

        public async Task<int> Count(ImageFilter ImageFilter)
        {
            IQueryable<ImageDAO> query = DataContext.Image.AsNoTracking();
            query = DynamicFilter(query, ImageFilter);
            return await query.CountAsync();
        }

        public async Task<List<Image>> List(ImageFilter ImageFilter)
        {
            IQueryable<ImageDAO> query = DataContext.Image.AsNoTracking();
            query = DynamicFilter(query, ImageFilter);
            query = DynamicOrder(query, ImageFilter);
            List<Image> Images = await DynamicSelect(query, ImageFilter);
            return Images;
        }

        public async Task<Image> Get(long Id)
        {
            ImageDAO ImageDAO = await DataContext.Image.Where(f => f.Id == Id).FirstOrDefaultAsync();
            Image Image = new Image
            {
                Id = ImageDAO.Id,
                Content = ImageDAO.Content,
                Name = ImageDAO.Name,
                Path = ImageDAO.Path,
                Url = ImageDAO.Url,
            };
            return Image;
        }

        public async Task<bool> Create(Image Image)
        {
            ImageDAO ImageDAO = new ImageDAO();
            ImageDAO.Id = Image.Id;
            ImageDAO.Name = Image.Name;
            ImageDAO.Url = Image.Url;
            ImageDAO.Path = Image.Path;
            ImageDAO.MimeType = Image.MimeType;
            ImageDAO.Content = Image.Content;
            await DataContext.Image.AddAsync(ImageDAO);
            await DataContext.SaveChangesAsync();

            Image.Id = await DataContext.Image.Where(f => f.Path == Image.Path).Select(f => f.Id).FirstOrDefaultAsync();
            return true;
        }
    }
}
