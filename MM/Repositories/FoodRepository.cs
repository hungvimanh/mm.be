using Common;
using MM.Entities;
using MM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Helpers;

namespace MM.Repositories
{
    public interface IFoodRepository
    {
        Task<int> Count(FoodFilter FoodFilter);
        Task<List<Food>> List(FoodFilter FoodFilter);
        Task<Food> Get(long Id);
        Task<bool> Create(Food Food);
        Task<bool> Update(Food Food);
        Task<bool> Delete(Food Food);
        Task<bool> Vote(FoodAccountMapping FoodAccountMapping);
    }
    public class FoodRepository : IFoodRepository
    {
        private DataContext DataContext;
        public FoodRepository(DataContext DataContext)
        {
            this.DataContext = DataContext;
        }

        private IQueryable<FoodDAO> DynamicFilter(IQueryable<FoodDAO> query, FoodFilter filter)
        {
            if (filter == null)
                return query.Where(q => false);
            query = query.Where(q => !q.DeletedAt.HasValue);
            if (filter.Id != null)
                query = query.Where(q => q.Id, filter.Id);
            if (filter.Name != null)
                query = query.Where(q => q.Name, filter.Name);
            if (filter.PriceEach != null)
                query = query.Where(q => q.PriceEach, filter.PriceEach);
            if (filter.DiscountRate != null)
                query = query.Where(q => q.DiscountRate, filter.DiscountRate);
            if (filter.StatusId != null)
                query = query.Where(q => q.StatusId, filter.StatusId);
            if (filter.Descreption != null)
                query = query.Where(q => q.Descreption, filter.Descreption);
            if (filter.Rate != null)
                query = query.Where(q => q.Rate, filter.Rate);
            if (filter.FoodGroupingId != null)
            {
                if (filter.FoodGroupingId.Equal.HasValue)
                {
                    FoodGroupingDAO FoodGroupingDAO = DataContext.FoodGrouping
                        .Where(pg => pg.Id == filter.FoodGroupingId.Equal.Value).FirstOrDefault();
                    query = from q in query
                            join ffg in DataContext.FoodFoodGroupingMapping on q.Id equals ffg.FoodId
                            join fg in DataContext.FoodGrouping on ffg.FoodGroupingId equals fg.Id
                            where fg.Id.Equals(FoodGroupingDAO.Id)
                            select q;
                }
            }
            return query;
        }

        private IQueryable<FoodDAO> DynamicOrder(IQueryable<FoodDAO> query, FoodFilter filter)
        {
            switch (filter.OrderType)
            {
                case OrderType.ASC:
                    switch (filter.OrderBy)
                    {
                        case FoodOrder.Id:
                            query = query.OrderBy(q => q.Id);
                            break;
                        case FoodOrder.Name:
                            query = query.OrderBy(q => q.Name);
                            break;
                        case FoodOrder.PriceEach:
                            query = query.OrderBy(q => q.PriceEach);
                            break;
                        case FoodOrder.DiscountRate:
                            query = query.OrderBy(q => q.DiscountRate);
                            break;
                        case FoodOrder.Image:
                            query = query.OrderBy(q => q.Image);
                            break;
                        case FoodOrder.Status:
                            query = query.OrderBy(q => q.StatusId);
                            break;
                        case FoodOrder.Descreption:
                            query = query.OrderBy(q => q.Descreption);
                            break;
                        case FoodOrder.CreatedAt:
                            query = query.OrderBy(q => q.CreatedAt);
                            break;
                        case FoodOrder.Rate:
                            query = query.OrderBy(q => q.Rate);
                            break;
                    }
                    break;
                case OrderType.DESC:
                    switch (filter.OrderBy)
                    {
                        case FoodOrder.Id:
                            query = query.OrderByDescending(q => q.Id);
                            break;
                        case FoodOrder.Name:
                            query = query.OrderByDescending(q => q.Name);
                            break;
                        case FoodOrder.PriceEach:
                            query = query.OrderByDescending(q => q.PriceEach);
                            break;
                        case FoodOrder.DiscountRate:
                            query = query.OrderByDescending(q => q.DiscountRate);
                            break;
                        case FoodOrder.Image:
                            query = query.OrderByDescending(q => q.Image);
                            break;
                        case FoodOrder.Status:
                            query = query.OrderByDescending(q => q.StatusId);
                            break;
                        case FoodOrder.Descreption:
                            query = query.OrderByDescending(q => q.Descreption);
                            break;
                        case FoodOrder.CreatedAt:
                            query = query.OrderByDescending(q => q.CreatedAt);
                            break;
                        case FoodOrder.Rate:
                            query = query.OrderByDescending(q => q.Rate);
                            break;
                    }
                    break;
            }
            query = query.Skip(filter.Skip).Take(filter.Take);
            return query;
        }

        private async Task<List<Food>> DynamicSelect(IQueryable<FoodDAO> query, FoodFilter filter)
        {
            List<Food> Foods = await query.Select(q => new Food()
            {
                Id = filter.Selects.Contains(FoodSelect.Id) ? q.Id : default(long),
                Name = filter.Selects.Contains(FoodSelect.Name) ? q.Name : default(string),
                PriceEach = filter.Selects.Contains(FoodSelect.PriceEach) ? q.PriceEach : default(decimal),
                DiscountRate = filter.Selects.Contains(FoodSelect.DiscountRate) ? q.DiscountRate : default(decimal?),
                ImageId = filter.Selects.Contains(FoodSelect.Image) ? q.ImageId : default(long?),
                StatusId = filter.Selects.Contains(FoodSelect.Status) ? q.StatusId : default(long),
                Descreption = filter.Selects.Contains(FoodSelect.Descreption) ? q.Descreption : default(string),
                Rate = filter.Selects.Contains(FoodSelect.Rate) ? q.Rate : default(long),
                Image = q.Image == null ? null : new Image
                {
                    Id = q.Image.Id,
                    Name = q.Image.Name,
                    Content = q.Image.Content,
                    Path = q.Image.Path,
                    Url = q.Image.Url,
                }
            }).AsNoTracking().ToListAsync();
            return Foods;
        }

        public async Task<int> Count(FoodFilter filter)
        {
            IQueryable<FoodDAO> Foods = DataContext.Food;
            Foods = DynamicFilter(Foods, filter);
            return await Foods.CountAsync();
        }

        public async Task<List<Food>> List(FoodFilter filter)
        {
            if (filter == null) return new List<Food>();
            IQueryable<FoodDAO> FoodDAOs = DataContext.Food;
            FoodDAOs = DynamicFilter(FoodDAOs, filter);
            FoodDAOs = DynamicOrder(FoodDAOs, filter);
            List<Food> Foods = await DynamicSelect(FoodDAOs, filter);
            return Foods;
        }

        public async Task<Food> Get(long Id)
        {
            Food Food = await DataContext.Food.Where(x => x.Id == Id).Select(x => new Food()
            {
                Id = x.Id,
                Name = x.Name,
                PriceEach = x.PriceEach,
                DiscountRate = x.DiscountRate,
                ImageId = x.ImageId,
                StatusId = x.StatusId,
                Descreption = x.Descreption,
                Rate = x.Rate,
                Image = x.Image == null ? null : new Image
                {
                    Id = x.Image.Id,
                    Name = x.Image.Name,
                    Content = x.Image.Content,
                    Path = x.Image.Path,
                    Url = x.Image.Url,
                }
            }).AsNoTracking().FirstOrDefaultAsync();

            if (Food == null)
                return null;

            Food.FoodFoodGroupingMappings = await DataContext.FoodFoodGroupingMapping.Where(x => x.FoodId == Id).Select(x => new FoodFoodGroupingMapping
            {
                FoodId = x.FoodId,
                FoodGroupingId = x.FoodGroupingId,
                FoodGrouping = new FoodGrouping
                {
                    Id = x.FoodGrouping.Id,
                    Name = x.FoodGrouping.Name,
                    StatusId = x.FoodGrouping.StatusId
                }
            }).ToListAsync();

            Food.FoodFoodTypeMappings = await DataContext.FoodFoodTypeMapping
                .Where(x => x.FoodId == Id)
                .Where(x => x.DeletedAt == null)
                .Select(x => new FoodFoodTypeMapping
            {
                Id = x.Id,
                FoodId = x.FoodId,
                FoodTypeId = x.FoodTypeId,
                FoodType = new FoodType
                {
                    Id = x.FoodType.Id,
                    Name = x.FoodType.Name,
                    StatusId = x.FoodType.StatusId
                }
            }).ToListAsync();

            Food.FoodAccountMappings = await DataContext.FoodAccountMapping.Where(x => x.FoodId == Id).Select(x => new FoodAccountMapping
            {
                FoodId = x.FoodId,
                AccountId = x.AccountId,
                Rate = x.Rate,
            }).ToListAsync();

            Food.Comments = await DataContext.Comment.Where(x => x.FoodId == Id).Select(x => new Comment
            {
                Id = x.Id,
                Content = x.Content,
                AccountId = x.AccountId,
                FoodId = x.FoodId,
                Time = x.Time,
                Account = x.Account == null ? null : new Account
                {
                    Id = x.Account.Id,
                    Email = x.Account.Email,
                    DisplayName = x.Account.DisplayName,
                    ImageId = x.Account.ImageId,
                    Image = x.Account.Image == null ? null : new Image
                    {
                        Id = x.Account.Image.Id,
                        Name = x.Account.Image.Name,
                        Content = x.Account.Image.Content,
                        Path = x.Account.Image.Path,
                        Url = x.Account.Image.Url,
                    },
                },
            }).ToListAsync();

            foreach (var Comment in Food.Comments)
            {
                Comment.FoodAccountMapping = Food.FoodAccountMappings.Where(x => x.AccountId == Comment.AccountId && x.FoodId == Comment.FoodId).FirstOrDefault();
            }
            return Food;
        }
        public async Task<bool> Create(Food Food)
        {
            FoodDAO FoodDAO = new FoodDAO();
            FoodDAO.Id = Food.Id;
            FoodDAO.Name = Food.Name;
            FoodDAO.PriceEach = Food.PriceEach;
            FoodDAO.DiscountRate = Food.DiscountRate;
            FoodDAO.ImageId = Food.ImageId;
            FoodDAO.StatusId = Food.StatusId;
            FoodDAO.Rate = 0;
            FoodDAO.Descreption = Food.Descreption;
            FoodDAO.CreatedAt = StaticParams.DateTimeNow;
            FoodDAO.UpdatedAt = StaticParams.DateTimeNow;
            DataContext.Food.Add(FoodDAO);
            await DataContext.SaveChangesAsync();
            Food.Id = FoodDAO.Id;
            await SaveReference(Food);
            return true;
        }

        public async Task<bool> Update(Food Food)
        {
            FoodDAO FoodDAO = DataContext.Food.Where(x => x.Id == Food.Id).FirstOrDefault();
            if (FoodDAO == null)
                return false;
            FoodDAO.Id = Food.Id;
            FoodDAO.Name = Food.Name;
            FoodDAO.PriceEach = Food.PriceEach;
            FoodDAO.DiscountRate = Food.DiscountRate;
            FoodDAO.ImageId = Food.ImageId;
            FoodDAO.StatusId = Food.StatusId;
            FoodDAO.Rate = Food.Rate;
            FoodDAO.Descreption = Food.Descreption;
            FoodDAO.UpdatedAt = StaticParams.DateTimeNow;
            await DataContext.SaveChangesAsync();
            Food.Id = FoodDAO.Id;
            await SaveReference(Food);
            return true;
        }

        public async Task<bool> Delete(Food Food)
        {
            await DataContext.Food.Where(x => x.Id == Food.Id).UpdateFromQueryAsync(x => new FoodDAO { DeletedAt = StaticParams.DateTimeNow });
            return true;
        }

        public async Task<bool> Vote(FoodAccountMapping FoodAccountMapping)
        {
            FoodAccountMappingDAO FoodAccountMappingDAO = DataContext.FoodAccountMapping
                .Where(x => x.AccountId == FoodAccountMapping.AccountId && x.FoodId == FoodAccountMapping.FoodId)
                .FirstOrDefault();
            if(FoodAccountMappingDAO == null)
            {
                FoodAccountMappingDAO = new FoodAccountMappingDAO
                {
                    FoodId = FoodAccountMapping.FoodId,
                    AccountId = FoodAccountMapping.AccountId,
                    Rate = FoodAccountMapping.Rate,
                };
                await DataContext.FoodAccountMapping.AddAsync(FoodAccountMappingDAO);
                
            }
            else
            {
                FoodAccountMappingDAO.Rate = FoodAccountMapping.Rate;
            }
            await DataContext.SaveChangesAsync();
            return true;
        }

        private async Task SaveReference(Food Food)
        {
            await DataContext.FoodFoodGroupingMapping.Where(x => x.FoodId == Food.Id).DeleteFromQueryAsync();
            if(Food.FoodFoodGroupingMappings != null)
            {
                List<FoodFoodGroupingMappingDAO> FoodFoodGroupingMappingDAOs = new List<FoodFoodGroupingMappingDAO>();
                foreach (var FoodFoodGroupingMapping in Food.FoodFoodGroupingMappings)
                {
                    FoodFoodGroupingMappingDAO FoodFoodGroupingMappingDAO = new FoodFoodGroupingMappingDAO
                    {
                        FoodId = Food.Id,
                        FoodGroupingId = FoodFoodGroupingMapping.FoodGroupingId
                    };
                    FoodFoodGroupingMappingDAOs.Add(FoodFoodGroupingMappingDAO);
                }
                await DataContext.FoodFoodGroupingMapping.BulkMergeAsync(FoodFoodGroupingMappingDAOs);
            }

            List<FoodFoodTypeMappingDAO> FoodFoodTypeMappingDAOs = await DataContext.FoodFoodTypeMapping.Where(x => x.FoodId == Food.Id).ToListAsync();
            FoodFoodTypeMappingDAOs.ForEach(x => x.DeletedAt = StaticParams.DateTimeNow);
            if (Food.FoodFoodTypeMappings != null)
            {
                foreach (var FoodFoodTypeMapping in Food.FoodFoodTypeMappings)
                {
                    FoodFoodTypeMappingDAO FoodFoodTypeMappingDAO = FoodFoodTypeMappingDAOs.Where(x => x.Id == FoodFoodTypeMapping.Id && x.Id != 0).FirstOrDefault();
                    if(FoodFoodTypeMappingDAO == null)
                    {
                        FoodFoodTypeMappingDAO = new FoodFoodTypeMappingDAO
                        {
                            FoodId = Food.Id,
                            FoodTypeId = FoodFoodTypeMapping.FoodTypeId,
                            DeletedAt = null
                        };
                        FoodFoodTypeMappingDAOs.Add(FoodFoodTypeMappingDAO);
                    }
                    else
                    {
                        FoodFoodTypeMappingDAO.Id = FoodFoodTypeMapping.Id;
                        FoodFoodTypeMappingDAO.FoodId = Food.Id;
                        FoodFoodTypeMappingDAO.FoodTypeId = FoodFoodTypeMapping.FoodTypeId;
                        FoodFoodTypeMappingDAO.DeletedAt = null;
                    }
                    
                }
                await DataContext.FoodFoodTypeMapping.BulkMergeAsync(FoodFoodTypeMappingDAOs);
            }

            await DataContext.FoodAccountMapping.Where(x => x.FoodId == Food.Id).DeleteFromQueryAsync();
            if (Food.FoodAccountMappings != null)
            {
                List<FoodAccountMappingDAO> FoodAccountMappingDAOs = new List<FoodAccountMappingDAO>();
                foreach (var FoodAccountMapping in Food.FoodAccountMappings)
                {
                    FoodAccountMappingDAO FoodAccountMappingDAO = new FoodAccountMappingDAO
                    {
                        FoodId = Food.Id,
                        AccountId = FoodAccountMapping.AccountId,
                        Rate = FoodAccountMapping.Rate
                    };
                    FoodAccountMappingDAOs.Add(FoodAccountMappingDAO);
                }
                await DataContext.FoodAccountMapping.BulkMergeAsync(FoodAccountMappingDAOs);
            }
            await DataContext.SaveChangesAsync();
        }
    }
}
