﻿using Microsoft.EntityFrameworkCore;
using MM.Entities;
using MM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Repositories
{
    public interface ICommentRepository
    {
        Task<Comment> Get(long Id);
        Task<bool> Create(Comment Comment);
    }
    public class CommentRepository : ICommentRepository
    {
        private DataContext DataContext;
        public CommentRepository(DataContext DataContext)
        {
            this.DataContext = DataContext;
        }

        public async Task<Comment> Get(long Id)
        {
            Comment Comment = await DataContext.Comment.Where(p => p.Id == Id)
               .Select(p => new Comment
               {
                   Id = p.Id,
                   Content = p.Content,
                   FoodId = p.FoodId,
                   Time = p.Time,
                   AccountId = p.AccountId,
                   Account = p.Account == null ? null : new Account
                   {
                       Id = p.Account.Id,
                       Email = p.Account.Email,
                       DisplayName = p.Account.DisplayName,
                   },
               }).FirstOrDefaultAsync();
            return Comment;
        }

        public async Task<bool> Create(Comment Comment)
        {
            CommentDAO CommentDAO = new CommentDAO
            {
                Content = Comment.Content,
                FoodId = Comment.FoodId,
                AccountId = Comment.AccountId,
                Time = Comment.Time,
            };
            DataContext.Comment.Add(CommentDAO);
            await DataContext.SaveChangesAsync();
            Comment.Id = CommentDAO.Id;
            return true;
        }
    }
}
