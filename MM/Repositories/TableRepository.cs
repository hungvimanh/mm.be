using Common;
using MM.Entities;
using MM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Helpers;

namespace MM.Repositories
{
    public interface ITableRepository
    {
        Task<int> Count(TableFilter TableFilter);
        Task<List<Table>> List(TableFilter TableFilter);
        Task<Table> Get(long Id);
        Task<bool> Create(Table Table);
        Task<bool> Update(Table Table);
        Task<bool> Delete(Table Table);
    }
    public class TableRepository : ITableRepository
    {
        private DataContext DataContext;
        public TableRepository(DataContext DataContext)
        {
            this.DataContext = DataContext;
        }

        private IQueryable<TableDAO> DynamicFilter(IQueryable<TableDAO> query, TableFilter filter)
        {
            if (filter == null)
                return query.Where(q => false);
            if (filter.Id != null)
                query = query.Where(q => q.Id, filter.Id);
            if (filter.Code != null)
                query = query.Where(q => q.Code, filter.Code);
            if (filter.StatusId != null)
                query = query.Where(q => q.StatusId, filter.StatusId);
            return query;
        }

        private IQueryable<TableDAO> DynamicOrder(IQueryable<TableDAO> query, TableFilter filter)
        {
            switch (filter.OrderType)
            {
                case OrderType.ASC:
                    switch (filter.OrderBy)
                    {
                        case TableOrder.Id:
                            query = query.OrderBy(q => q.Id);
                            break;
                        case TableOrder.Code:
                            query = query.OrderBy(q => q.Code);
                            break;
                        case TableOrder.Status:
                            query = query.OrderBy(q => q.StatusId);
                            break;
                    }
                    break;
                case OrderType.DESC:
                    switch (filter.OrderBy)
                    {
                        case TableOrder.Id:
                            query = query.OrderByDescending(q => q.Id);
                            break;
                        case TableOrder.Code:
                            query = query.OrderByDescending(q => q.Code);
                            break;
                        case TableOrder.Status:
                            query = query.OrderByDescending(q => q.StatusId);
                            break;
                    }
                    break;
            }
            query = query.Skip(filter.Skip).Take(filter.Take);
            return query;
        }

        private async Task<List<Table>> DynamicSelect(IQueryable<TableDAO> query, TableFilter filter)
        {
            List<Table> Tables = await query.Select(q => new Table()
            {
                Id = filter.Selects.Contains(TableSelect.Id) ? q.Id : default(long),
                Code = filter.Selects.Contains(TableSelect.Code) ? q.Code : default(string),
                StatusId = filter.Selects.Contains(TableSelect.Status) ? q.StatusId : default(long),
            }).AsNoTracking().ToListAsync();
            return Tables;
        }

        public async Task<int> Count(TableFilter filter)
        {
            IQueryable<TableDAO> Tables = DataContext.Table;
            Tables = DynamicFilter(Tables, filter);
            return await Tables.CountAsync();
        }

        public async Task<List<Table>> List(TableFilter filter)
        {
            if (filter == null) return new List<Table>();
            IQueryable<TableDAO> TableDAOs = DataContext.Table;
            TableDAOs = DynamicFilter(TableDAOs, filter);
            TableDAOs = DynamicOrder(TableDAOs, filter);
            List<Table> Tables = await DynamicSelect(TableDAOs, filter);
            return Tables;
        }

        public async Task<Table> Get(long Id)
        {
            Table Table = await DataContext.Table.Where(x => x.Id == Id).Select(x => new Table()
            {
                Id = x.Id,
                Code = x.Code,
                StatusId = x.StatusId
            }).AsNoTracking().FirstOrDefaultAsync();

            if (Table == null)
                return null;

            Table.Reservations = await DataContext.Reservation.Where(x => x.TableId == Id).Select(x => new Reservation
            {
                Id = x.Id,
                TableId = x.TableId,
                OrderId = x.OrderId,
                Date = x.Date,
                StatusId = x.StatusId,
            }).ToListAsync();
            return Table;
        }
        public async Task<bool> Create(Table Table)
        {
            TableDAO TableDAO = new TableDAO();
            TableDAO.Id = Table.Id;
            TableDAO.Code = Table.Code;
            TableDAO.StatusId = Table.StatusId;
            DataContext.Table.Add(TableDAO);
            await DataContext.SaveChangesAsync();
            Table.Id = TableDAO.Id;
            return true;
        }

        public async Task<bool> Update(Table Table)
        {
            TableDAO TableDAO = DataContext.Table.Where(x => x.Id == Table.Id).FirstOrDefault();
            if (TableDAO == null)
                return false;
            TableDAO.Id = Table.Id;
            TableDAO.Code = Table.Code;
            TableDAO.StatusId = Table.StatusId;
            await DataContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Delete(Table Table)
        {
            await DataContext.Reservation.Where(x => x.TableId == Table.Id).DeleteFromQueryAsync();
            await DataContext.Table.Where(x => x.Id == Table.Id).DeleteFromQueryAsync();
            return true;
        }
    }
}
