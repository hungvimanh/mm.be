﻿using MM.Entities;
using MM.Models;
using System;
using Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MM.Repositories
{
    public interface IFoodFoodTypeMappingRepository
    {
        Task<int> Count(FoodFoodTypeMappingFilter filter);
        Task<List<FoodFoodTypeMapping>> List(FoodFoodTypeMappingFilter filter);
        Task<FoodFoodTypeMapping> Get(long Id);
    }
    public class FoodFoodTypeMappingRepository : IFoodFoodTypeMappingRepository
    {
        private DataContext DataContext;
        public FoodFoodTypeMappingRepository(DataContext DataContext)
        {
            this.DataContext = DataContext;
        }

        private IQueryable<FoodFoodTypeMappingDAO> DynamicFilter(IQueryable<FoodFoodTypeMappingDAO> query, FoodFoodTypeMappingFilter filter)
        {
            if (filter == null)
                return query.Where(q => false);
            if (filter.Id != null)
                query = query.Where(q => q.Id, filter.Id);
            if (filter.FoodId != null)
                query = query.Where(q => q.FoodId, filter.FoodId);
            if (filter.FoodTypeId != null)
                query = query.Where(q => q.FoodTypeId, filter.FoodTypeId);
            return query;
        }

        private IQueryable<FoodFoodTypeMappingDAO> DynamicOrder(IQueryable<FoodFoodTypeMappingDAO> query, FoodFoodTypeMappingFilter filter)
        {
            switch (filter.OrderType)
            {
                case OrderType.ASC:
                    switch (filter.OrderBy)
                    {
                        case FoodFoodTypeMappingOrder.Id:
                            query = query.OrderBy(q => q.Id);
                            break;
                        case FoodFoodTypeMappingOrder.Food:
                            query = query.OrderBy(q => q.FoodId);
                            break;
                        case FoodFoodTypeMappingOrder.FoodType:
                            query = query.OrderBy(q => q.FoodTypeId);
                            break;
                    }
                    break;
                case OrderType.DESC:
                    switch (filter.OrderBy)
                    {
                        case FoodFoodTypeMappingOrder.Id:
                            query = query.OrderByDescending(q => q.Id);
                            break;
                        case FoodFoodTypeMappingOrder.Food:
                            query = query.OrderByDescending(q => q.FoodId);
                            break;
                        case FoodFoodTypeMappingOrder.FoodType:
                            query = query.OrderByDescending(q => q.FoodTypeId);
                            break;
                    }
                    break;
            }
            query = query.Skip(filter.Skip).Take(filter.Take);
            return query;
        }

        private async Task<List<FoodFoodTypeMapping>> DynamicSelect(IQueryable<FoodFoodTypeMappingDAO> query, FoodFoodTypeMappingFilter filter)
        {
            List<FoodFoodTypeMapping> FoodFoodTypeMappings = await query.Select(q => new FoodFoodTypeMapping()
            {
                Id = filter.Selects.Contains(FoodFoodTypeMappingSelect.Id) ? q.Id : default(long),
                FoodId = filter.Selects.Contains(FoodFoodTypeMappingSelect.Food) ? q.FoodId : default(long),
                FoodTypeId = filter.Selects.Contains(FoodFoodTypeMappingSelect.FoodType) ? q.FoodTypeId : default(long),
                Food = filter.Selects.Contains(FoodFoodTypeMappingSelect.Food) && q.Food == null ? null : new Food
                {
                    Id = q.Food.Id,
                    Name = q.Food.Name,
                    Descreption = q.Food.Descreption,
                    DiscountRate = q.Food.DiscountRate,
                    PriceEach = q.Food.PriceEach,
                    StatusId = q.Food.StatusId,
                },
                FoodType = filter.Selects.Contains(FoodFoodTypeMappingSelect.FoodType) && q.FoodType == null ? null : new FoodType
                {
                    Id = q.FoodType.Id,
                    Name = q.FoodType.Name,
                    StatusId = q.FoodType.StatusId,
                }
            }).AsNoTracking().ToListAsync();
            return FoodFoodTypeMappings;
        }

        public async Task<int> Count(FoodFoodTypeMappingFilter filter)
        {
            IQueryable<FoodFoodTypeMappingDAO> FoodFoodTypeMappings = DataContext.FoodFoodTypeMapping;
            FoodFoodTypeMappings = DynamicFilter(FoodFoodTypeMappings, filter);
            return await FoodFoodTypeMappings.CountAsync();
        }

        public async Task<List<FoodFoodTypeMapping>> List(FoodFoodTypeMappingFilter filter)
        {
            if (filter == null) return new List<FoodFoodTypeMapping>();
            IQueryable<FoodFoodTypeMappingDAO> FoodFoodTypeMappingDAOs = DataContext.FoodFoodTypeMapping;
            FoodFoodTypeMappingDAOs = DynamicFilter(FoodFoodTypeMappingDAOs, filter);
            FoodFoodTypeMappingDAOs = DynamicOrder(FoodFoodTypeMappingDAOs, filter);
            List<FoodFoodTypeMapping> FoodFoodTypeMappings = await DynamicSelect(FoodFoodTypeMappingDAOs, filter);
            return FoodFoodTypeMappings;
        }

        public async Task<FoodFoodTypeMapping> Get(long Id)
        {
            FoodFoodTypeMapping FoodFoodTypeMapping = await DataContext.FoodFoodTypeMapping.Where(x => x.Id == Id).Select(x => new FoodFoodTypeMapping()
            {
                Id = x.Id,
                FoodId = x.FoodId,
                FoodTypeId = x.FoodTypeId,
                Food = new Food
                {
                    Id = x.Food.Id,
                    Name = x.Food.Name,
                    Descreption = x.Food.Descreption,
                    DiscountRate = x.Food.DiscountRate,
                    PriceEach = x.Food.PriceEach,
                    StatusId = x.Food.StatusId,
                },
                FoodType = new FoodType
                {
                    Id = x.FoodType.Id,
                    Name = x.FoodType.Name,
                    StatusId = x.FoodType.StatusId,
                }
            }).AsNoTracking().FirstOrDefaultAsync();

            if (FoodFoodTypeMapping == null)
                return null;

            return FoodFoodTypeMapping;
        }
    }
}
