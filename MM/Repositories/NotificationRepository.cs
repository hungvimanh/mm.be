using Common;
using MM.Entities;
using MM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Helpers;

namespace MM.Repositories
{
    public interface INotificationRepository
    {
        Task<int> Count(NotificationFilter NotificationFilter);
        Task<List<Notification>> List(NotificationFilter NotificationFilter);
        Task<Notification> Get(long Id);
        Task<bool> Create(Notification Notification);
        Task<bool> Update(Notification Notification);
        Task<bool> Delete(Notification Notification);
        Task<bool> BulkDelete(List<Notification> Notification);
    }
    public class NotificationRepository : INotificationRepository
    {
        private DataContext DataContext;
        public NotificationRepository(DataContext DataContext)
        {
            this.DataContext = DataContext;
        }

        private IQueryable<NotificationDAO> DynamicFilter(IQueryable<NotificationDAO> query, NotificationFilter filter)
        {
            if (filter == null)
                return query.Where(q => false);
            if (filter.Id != null)
                query = query.Where(q => q.Id, filter.Id);
            if (filter.AccountId != null)
                query = query.Where(q => q.AccountId, filter.AccountId);
            if (filter.Content != null)
                query = query.Where(q => q.Content, filter.Content);
            if (filter.Time != null)
                query = query.Where(q => q.Time, filter.Time);
            return query;
        }

        private IQueryable<NotificationDAO> DynamicOrder(IQueryable<NotificationDAO> query, NotificationFilter filter)
        {
            switch (filter.OrderType)
            {
                case OrderType.ASC:
                    switch (filter.OrderBy)
                    {
                        case NotificationOrder.Id:
                            query = query.OrderBy(q => q.Id);
                            break;
                        case NotificationOrder.Account:
                            query = query.OrderBy(q => q.AccountId);
                            break;
                        case NotificationOrder.Content:
                            query = query.OrderBy(q => q.Content);
                            break;
                        case NotificationOrder.Time:
                            query = query.OrderBy(q => q.Time);
                            break;
                        case NotificationOrder.Unread:
                            query = query.OrderBy(q => q.Unread);
                            break;
                    }
                    break;
                case OrderType.DESC:
                    switch (filter.OrderBy)
                    {
                        case NotificationOrder.Id:
                            query = query.OrderByDescending(q => q.Id);
                            break;
                        case NotificationOrder.Account:
                            query = query.OrderByDescending(q => q.AccountId);
                            break;
                        case NotificationOrder.Content:
                            query = query.OrderByDescending(q => q.Content);
                            break;
                        case NotificationOrder.Time:
                            query = query.OrderByDescending(q => q.Time);
                            break;
                        case NotificationOrder.Unread:
                            query = query.OrderByDescending(q => q.Unread);
                            break;
                    }
                    break;
            }
            query = query.Skip(filter.Skip).Take(filter.Take);
            return query;
        }

        private async Task<List<Notification>> DynamicSelect(IQueryable<NotificationDAO> query, NotificationFilter filter)
        {
            List<Notification> Notifications = await query.Select(q => new Notification()
            {
                Id = filter.Selects.Contains(NotificationSelect.Id) ? q.Id : default(long),
                AccountId = filter.Selects.Contains(NotificationSelect.Account) ? q.AccountId : default(long),
                Content = filter.Selects.Contains(NotificationSelect.Content) ? q.Content : default(string),
                Time = filter.Selects.Contains(NotificationSelect.Time) ? q.Time : default(DateTime),
                Unread = filter.Selects.Contains(NotificationSelect.Unread) ? q.Unread : default(bool),
                Account = filter.Selects.Contains(NotificationSelect.Account) && q.Account != null ? new Account
                {
                    Id = q.Account.Id,
                    DisplayName = q.Account.DisplayName,
                    Email = q.Account.Email,
                    Phone = q.Account.Phone,
                    Password = q.Account.Password,
                    Salt = q.Account.Salt,
                    PasswordRecoveryCode = q.Account.PasswordRecoveryCode,
                    ExpiredTimeCode = q.Account.ExpiredTimeCode,
                    Address = q.Account.Address,
                    Dob = q.Account.Dob,
                    ImageId = q.Account.ImageId,
                    RoleId = q.Account.RoleId,
                    Image = q.Account.Image == null ? null : new Image
                    {
                        Id = q.Account.Image.Id,
                        Name = q.Account.Image.Name,
                        Content = q.Account.Image.Content,
                        Path = q.Account.Image.Path,
                        Url = q.Account.Image.Url,
                    }
                } : null,
            }).AsNoTracking().ToListAsync();
            return Notifications;
        }

        public async Task<int> Count(NotificationFilter filter)
        {
            IQueryable<NotificationDAO> Notifications = DataContext.Notification;
            Notifications = DynamicFilter(Notifications, filter);
            return await Notifications.CountAsync();
        }

        public async Task<List<Notification>> List(NotificationFilter filter)
        {
            if (filter == null) return new List<Notification>();
            IQueryable<NotificationDAO> NotificationDAOs = DataContext.Notification;
            NotificationDAOs = DynamicFilter(NotificationDAOs, filter);
            NotificationDAOs = DynamicOrder(NotificationDAOs, filter);
            List<Notification> Notifications = await DynamicSelect(NotificationDAOs, filter);
            return Notifications;
        }

        public async Task<Notification> Get(long Id)
        {
            Notification Notification = await DataContext.Notification.Where(x => x.Id == Id).Select(x => new Notification()
            {
                Id = x.Id,
                AccountId = x.AccountId,
                Content = x.Content,
                Time = x.Time,
                Unread = x.Unread,
                Account = x.Account == null ? null : new Account
                {
                    Id = x.Account.Id,
                    DisplayName = x.Account.DisplayName,
                    Email = x.Account.Email,
                    Phone = x.Account.Phone,
                    Password = x.Account.Password,
                    Salt = x.Account.Salt,
                    PasswordRecoveryCode = x.Account.PasswordRecoveryCode,
                    ExpiredTimeCode = x.Account.ExpiredTimeCode,
                    Address = x.Account.Address,
                    Dob = x.Account.Dob,
                    ImageId = x.Account.ImageId,
                    RoleId = x.Account.RoleId,
                    Image = x.Account.Image == null ? null : new Image
                    {
                        Id = x.Account.Image.Id,
                        Name = x.Account.Image.Name,
                        Content = x.Account.Image.Content,
                        Path = x.Account.Image.Path,
                        Url = x.Account.Image.Url,
                    }
                },
            }).AsNoTracking().FirstOrDefaultAsync();

            if (Notification == null)
                return null;

            return Notification;
        }
        public async Task<bool> Create(Notification Notification)
        {
            NotificationDAO NotificationDAO = new NotificationDAO();
            NotificationDAO.Id = Notification.Id;
            NotificationDAO.AccountId = Notification.AccountId;
            NotificationDAO.Content = Notification.Content;
            NotificationDAO.Time = DateTime.Now.AddHours(7);
            NotificationDAO.Unread = Notification.Unread;
            DataContext.Notification.Add(NotificationDAO);
            await DataContext.SaveChangesAsync();
            Notification.Id = NotificationDAO.Id;
            await SaveReference(Notification);
            return true;
        }

        public async Task<bool> Update(Notification Notification)
        {
            NotificationDAO NotificationDAO = DataContext.Notification.Where(x => x.Id == Notification.Id).FirstOrDefault();
            if (NotificationDAO == null)
                return false;
            NotificationDAO.Id = Notification.Id;
            NotificationDAO.AccountId = Notification.AccountId;
            NotificationDAO.Content = Notification.Content;
            NotificationDAO.Time = Notification.Time;
            NotificationDAO.Unread = Notification.Unread;
            await DataContext.SaveChangesAsync();
            await SaveReference(Notification);
            return true;
        }

        public async Task<bool> Delete(Notification Notification)
        {
            await DataContext.Notification.Where(x => x.Id == Notification.Id).DeleteFromQueryAsync();
            return true;
        }
        
        public async Task<bool> BulkMerge(List<Notification> Notifications)
        {
            List<NotificationDAO> NotificationDAOs = new List<NotificationDAO>();
            foreach (Notification Notification in Notifications)
            {
                NotificationDAO NotificationDAO = new NotificationDAO();
                NotificationDAO.Id = Notification.Id;
                NotificationDAO.AccountId = Notification.AccountId;
                NotificationDAO.Content = Notification.Content;
                NotificationDAO.Time = Notification.Time;
                NotificationDAO.Unread = Notification.Unread;
                NotificationDAOs.Add(NotificationDAO);
            }
            await DataContext.BulkMergeAsync(NotificationDAOs);
            return true;
        }

        public async Task<bool> BulkDelete(List<Notification> Notifications)
        {
            List<long> Ids = Notifications.Select(x => x.Id).ToList();
            await DataContext.Notification
                .Where(x => Ids.Contains(x.Id)).DeleteFromQueryAsync();
            return true;
        }

        private async Task SaveReference(Notification Notification)
        {
        }
        
    }
}
