﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Entities
{
    public class Comment : DataEntity
    {
        public long Id { get; set; }
        public long FoodId { get; set; }
        public string Content { get; set; }
        public DateTime Time { get; set; }
        public long AccountId { get; set; }
        public Account Account { get; set; }
        public FoodAccountMapping FoodAccountMapping { get; set; }
    }
}
