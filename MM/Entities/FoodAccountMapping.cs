﻿using Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Entities
{
    public class FoodAccountMapping : DataEntity, IEquatable<FoodAccountMapping>
    {
        public long FoodId { get; set; }
        public long AccountId { get; set; }
        public long? Rate { get; set; }
        public Food Food { get; set; }
        public Account Account { get; set; }

        public bool Equals(FoodAccountMapping other)
        {
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class FoodAccountMappingFilter : FilterEntity
    {
        public IdFilter FoodId { get; set; }
        public IdFilter AccountId { get; set; }
        public LongFilter Rate { get; set; }
        public FoodFoodGroupingMappingOrder OrderBy { get; set; }
        public FoodFoodGroupingMappingSelect Selects { get; set; }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum FoodAccountMappingOrder
    {
        Food = 0,
        Account = 1,
        Rate = 2,
    }

    [Flags]
    public enum FoodAccountMappingSelect : long
    {
        ALL = E.ALL,
        Food = E._0,
        Account = E._1,
        Rate = E._2,
    }
}
