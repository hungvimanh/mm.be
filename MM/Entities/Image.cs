﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MM.Entities
{
    public class Image : DataEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
        public string MimeType
        {
            get
            {
                FileInfo fileInfo = new FileInfo(Path);
                if (fileInfo.Extension.ToLower() == "jpg" || fileInfo.Extension.ToLower() == "jpeg")
                    return "image/jpeg";
                if (fileInfo.Extension.ToLower() == "png")
                    return "image/png";
                return "application/octet-stream";
            }
        }
        public string Path { get; set; }
        public string Url { get; set; }
    }

    public class ImageFilter : FilterEntity
    {
        public IdFilter Id { get; set; }
        public StringFilter Path { get; set; }
        public StringFilter Name { get; set; }
        public StringFilter Url { get; set; }
        public ImageOrder OrderBy { get; set; }
        public ImageSelect Selects { get; set; }
    }

    public enum ImageOrder
    {
        Id = 1,
        Path = 2,
        Name = 3,
        Url = 4,
    }

    [Flags]
    public enum ImageSelect : long
    {
        ALL = E.ALL,
        Id = E._0,
        Name = E._1,
        Url = E._2,
        Path = E._3,
    }
}
