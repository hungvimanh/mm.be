using Common;
using Helpers;
using MM.Entities;
using MM.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MM.Services.MOrderContent
{
    public interface IOrderContentService :  IServiceScoped
    {
        Task<int> Count(OrderContentFilter OrderContentFilter);
        Task<List<OrderContent>> List(OrderContentFilter OrderContentFilter);
        Task<OrderContent> Get(long Id);
        Task<OrderContent> Create(OrderContent OrderContent);
        Task<OrderContent> Update(OrderContent OrderContent);
        Task<OrderContent> Delete(OrderContent OrderContent);
    }

    public class OrderContentService : IOrderContentService
    {
        private IUOW UOW;
        private ICurrentContext CurrentContext;
        private IOrderContentValidator OrderContentValidator;

        public OrderContentService(
            IUOW UOW,
            ICurrentContext CurrentContext,
            IOrderContentValidator OrderContentValidator
        )
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
            this.OrderContentValidator = OrderContentValidator;
        }
        public async Task<int> Count(OrderContentFilter OrderContentFilter)
        {
            try
            {
                int result = await UOW.OrderContentRepository.Count(OrderContentFilter);
                return result;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<List<OrderContent>> List(OrderContentFilter OrderContentFilter)
        {
            try
            {
                List<OrderContent> OrderContents = await UOW.OrderContentRepository.List(OrderContentFilter);
                return OrderContents;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }
        public async Task<OrderContent> Get(long Id)
        {
            OrderContent OrderContent = await UOW.OrderContentRepository.Get(Id);
            if (OrderContent == null)
                return null;
            return OrderContent;
        }
       
        public async Task<OrderContent> Create(OrderContent OrderContent)
        {
            if (!await OrderContentValidator.Create(OrderContent))
                return OrderContent;

            try
            {
                await UOW.Begin();
                await UOW.OrderContentRepository.Create(OrderContent);
                await UOW.Commit();

                return await UOW.OrderContentRepository.Get(OrderContent.Id);
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<OrderContent> Update(OrderContent OrderContent)
        {
            if (!await OrderContentValidator.Update(OrderContent))
                return OrderContent;
            try
            {
                var oldData = await UOW.OrderContentRepository.Get(OrderContent.Id);

                await UOW.Begin();
                await UOW.OrderContentRepository.Update(OrderContent);
                await UOW.Commit();

                var newData = await UOW.OrderContentRepository.Get(OrderContent.Id);
                return newData;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<OrderContent> Delete(OrderContent OrderContent)
        {
            if (!await OrderContentValidator.Delete(OrderContent))
                return OrderContent;

            try
            {
                await UOW.Begin();
                await UOW.OrderContentRepository.Delete(OrderContent);
                await UOW.Commit();
                return OrderContent;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }
    }
}
