﻿using Common;
using Helpers;
using MM.Entities;
using MM.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Services.MCommentService
{
    public interface ICommentService : IServiceScoped
    {
        Task<Comment> Create(Comment Comment);
    }
    public class CommentService : ICommentService
    {
        private IUOW UOW;
        private ICurrentContext CurrentContext;
        public CommentService(IUOW UOW, ICurrentContext CurrentContext)
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
        }

        public async Task<Comment> Create(Comment Comment)
        {
            Comment.AccountId = CurrentContext.AccountId;
            Comment.Time = StaticParams.DateTimeNow;
            await UOW.CommentRepository.Create(Comment);
            return await UOW.CommentRepository.Get(Comment.Id);
        }
    }
}
