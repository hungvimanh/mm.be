using Common;
using Helpers;
using MM.Entities;
using MM.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Services.MFood
{
    public interface IFoodService :  IServiceScoped
    {
        Task<int> Count(FoodFilter FoodFilter);
        Task<List<Food>> List(FoodFilter FoodFilter);
        Task<Food> Get(long Id);
        Task<Food> Create(Food Food);
        Task<Food> Update(Food Food);
        Task<Food> Delete(Food Food);
        Task<Food> Vote(FoodAccountMapping FoodAccountMapping);
    }

    public class FoodService : IFoodService
    {
        private IUOW UOW;
        private ICurrentContext CurrentContext;
        private IFoodValidator FoodValidator;

        public FoodService(
            IUOW UOW,
            ICurrentContext CurrentContext,
            IFoodValidator FoodValidator
        )
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
            this.FoodValidator = FoodValidator;
        }
        public async Task<int> Count(FoodFilter FoodFilter)
        {
            try
            {
                int result = await UOW.FoodRepository.Count(FoodFilter);
                return result;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<List<Food>> List(FoodFilter FoodFilter)
        {
            try
            {
                List<Food> Foods = await UOW.FoodRepository.List(FoodFilter);
                return Foods;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }
        public async Task<Food> Get(long Id)
        {
            Food Food = await UOW.FoodRepository.Get(Id);
            if (Food == null)
                return null;
            if (Food.FoodAccountMappings != null)
            {
                Food.Rate = Math.Round(Convert.ToDecimal(Food.FoodAccountMappings.Average(x => x.Rate)), 1);
            }

            FoodAccountMapping FoodAccountMapping = Food.FoodAccountMappings.Where(x => x.FoodId == Id && x.AccountId == CurrentContext.AccountId).FirstOrDefault();
            if(FoodAccountMapping != null)
            {
                Food.MyRate = FoodAccountMapping.Rate;
            }
            return Food;
        }
       
        public async Task<Food> Create(Food Food)
        {
            if (!await FoodValidator.Create(Food))
                return Food;

            try
            {
                await UOW.Begin();
                await UOW.FoodRepository.Create(Food);
                await UOW.Commit();

                return await UOW.FoodRepository.Get(Food.Id);
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Food> Update(Food Food)
        {
            if (!await FoodValidator.Update(Food))
                return Food;
            try
            {
                var oldData = await UOW.FoodRepository.Get(Food.Id);

                await UOW.Begin();
                await UOW.FoodRepository.Update(Food);
                await UOW.Commit();

                var newData = await UOW.FoodRepository.Get(Food.Id);
                return newData;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Food> Delete(Food Food)
        {
            if (!await FoodValidator.Delete(Food))
                return Food;

            try
            {
                await UOW.Begin();
                await UOW.FoodRepository.Delete(Food);
                await UOW.Commit();
                return Food;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Food> Vote(FoodAccountMapping FoodAccountMapping)
        {
            try
            {
                await UOW.Begin();
                await UOW.FoodRepository.Vote(FoodAccountMapping);
                await UOW.Commit();
                return await Get(FoodAccountMapping.FoodId);
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }
    }
}
