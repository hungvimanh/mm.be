using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using MM.Entities;
using MM;
using MM.Repositories;

namespace MM.Services.MFood
{
    public interface IFoodValidator : IServiceScoped
    {
        Task<bool> Create(Food Food);
        Task<bool> Update(Food Food);
        Task<bool> Delete(Food Food);
        Task<bool> BulkDelete(List<Food> Foods);
        Task<bool> Import(List<Food> Foods);
    }

    public class FoodValidator : IFoodValidator
    {
        public enum ErrorCode
        {
            IdNotExisted,
            NameEmpty,
            NameOverLength,
            NameExisted,
            PriceEachEmpty,
            StatusIdNotExisted
        }

        private IUOW UOW;
        private ICurrentContext CurrentContext;

        public FoodValidator(IUOW UOW, ICurrentContext CurrentContext)
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
        }

        public async Task<bool> ValidateId(Food Food)
        {
            FoodFilter FoodFilter = new FoodFilter
            {
                Skip = 0,
                Take = 10,
                Id = new IdFilter { Equal = Food.Id },
                Selects = FoodSelect.Id
            };

            int count = await UOW.FoodRepository.Count(FoodFilter);
            if (count == 0)
                Food.AddError(nameof(FoodValidator), nameof(Food.Id), ErrorCode.IdNotExisted);
            return count == 1;
        }

        public async Task<bool> ValidateName(Food Food)
        {
            if (string.IsNullOrWhiteSpace(Food.Name))
            {
                Food.AddError(nameof(FoodValidator), nameof(Food.Name), ErrorCode.NameEmpty);
            }
            else
            {
                if(Food.Name.Length > 255)
                    Food.AddError(nameof(FoodValidator), nameof(Food.Name), ErrorCode.NameOverLength);
                FoodFilter filter = new FoodFilter
                {
                    Id = new IdFilter { NotEqual = Food.Id },
                    Name = new StringFilter { Equal = Food.Name }
                };
                int count = await UOW.FoodRepository.Count(filter);
                if(count > 0)
                    Food.AddError(nameof(FoodValidator), nameof(Food.Name), ErrorCode.NameExisted);
            }
            return Food.IsValidated;
        }

        public async Task<bool> ValidatePriceEach(Food Food)
        {
            if(Food.PriceEach == 0)
                Food.AddError(nameof(FoodValidator), nameof(Food.PriceEach), ErrorCode.PriceEachEmpty);
            return Food.IsValidated;
        }

        public async Task<bool> ValidateStatus(Food Food)
        {
            if(Food.StatusId != Enums.FoodStatusEnum.ACTIVE.Id && Food.StatusId != Enums.FoodStatusEnum.INACTIVE.Id)
                Food.AddError(nameof(FoodValidator), nameof(Food.StatusId), ErrorCode.StatusIdNotExisted);
            return Food.IsValidated;
        }

        public async Task<bool>Create(Food Food)
        {
            await ValidateName(Food);
            await ValidatePriceEach(Food);
            await ValidateStatus(Food);
            return Food.IsValidated;
        }

        public async Task<bool> Update(Food Food)
        {
            if (await ValidateId(Food))
            {
                await ValidateName(Food);
                await ValidatePriceEach(Food);
                await ValidateStatus(Food);
            }
            return Food.IsValidated;
        }

        public async Task<bool> Delete(Food Food)
        {
            if (await ValidateId(Food))
            {
            }
            return Food.IsValidated;
        }
        
        public async Task<bool> BulkDelete(List<Food> Foods)
        {
            return true;
        }
        
        public async Task<bool> Import(List<Food> Foods)
        {
            return true;
        }
    }
}
