using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using MM.Entities;
using MM;
using MM.Repositories;
using Helpers;

namespace MM.Services.MOrder
{
    public interface IOrderValidator : IServiceScoped
    {
        Task<bool> Create(Order Order);
        Task<bool> Update(Order Order);
        Task<bool> Delete(Order Order);
        Task<bool> Approve(Order Order);
        Task<bool> Reject(Order Order);
        Task<bool> Done(Order Order);
        Task<bool> BulkDelete(List<Order> Orders);
        Task<bool> Import(List<Order> Orders);
    }

    public class OrderValidator : IOrderValidator
    {
        public enum ErrorCode
        {
            IdNotExisted,
            OrderDateEmpty,
            NumOfPersonEmpty,
            NumOfTableEmpty,
            ContentEmpty,
            DescreptionOverLength,
            StatusNotPending,
            StatusNotApproved,
            ReservationsEmpty,
            OrderDateInvalid,
            OrderPending,
            OrderDone,
            OrderApproved,
            OrderReject,
            ReservationsInvalid
        }

        private IUOW UOW;
        private ICurrentContext CurrentContext;

        public OrderValidator(IUOW UOW, ICurrentContext CurrentContext)
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
        }

        public async Task<bool> ValidateId(Order Order)
        {
            OrderFilter OrderFilter = new OrderFilter
            {
                Skip = 0,
                Take = 10,
                Id = new IdFilter { Equal = Order.Id },
                Selects = OrderSelect.Id
            };

            int count = await UOW.OrderRepository.Count(OrderFilter);
            if (count == 0)
                Order.AddError(nameof(OrderValidator), nameof(Order.Id), ErrorCode.IdNotExisted);
            return count == 1;
        }

        private async Task<bool> ValidateOrderDate(Order Order)
        {
            if(Order.OrderDate == DateTime.MinValue || Order.OrderDate == default(DateTime))
                Order.AddError(nameof(OrderValidator), nameof(Order.OrderDate), ErrorCode.OrderDateEmpty);
            else if(Order.OrderDate < StaticParams.DateTimeNow)
                Order.AddError(nameof(OrderValidator), nameof(Order.OrderDate), ErrorCode.OrderDateInvalid);
            return Order.IsValidated;
        }

        private async Task<bool> ValidateQuantity(Order Order)
        {
            if(Order.NumOfPerson <= 0)
                Order.AddError(nameof(OrderValidator), nameof(Order.NumOfPerson), ErrorCode.NumOfPersonEmpty);
            if (Order.NumOfTable <= 0)
                Order.AddError(nameof(OrderValidator), nameof(Order.NumOfTable), ErrorCode.NumOfTableEmpty);
            return Order.IsValidated;
        }

        private async Task<bool> ValidateContent(Order Order)
        {
            if(Order.OrderContents == null || !Order.OrderContents.Any())
                Order.AddError(nameof(OrderValidator), nameof(Order.OrderContents), ErrorCode.ContentEmpty);
            return Order.IsValidated;
        }

        private async Task<bool> ValidateNote(Order Order)
        {
            if(!string.IsNullOrWhiteSpace(Order.Descreption) && Order.Descreption.Length > 255)
                Order.AddError(nameof(OrderValidator), nameof(Order.Descreption), ErrorCode.DescreptionOverLength);
            return Order.IsValidated;
        }

        public async Task<bool>Create(Order Order)
        {
            await ValidateOrderDate(Order);
            await ValidateQuantity(Order);
            await ValidateContent(Order);
            await ValidateNote(Order);
            return Order.IsValidated;
        }

        public async Task<bool> Update(Order Order)
        {
            if (await ValidateId(Order))
            {
                await ValidateOrderDate(Order);
                await ValidateQuantity(Order);
                await ValidateContent(Order);
                await ValidateNote(Order);
            }
            return Order.IsValidated;
        }

        public async Task<bool> Delete(Order Order)
        {
            if (await ValidateId(Order))
            {
            }
            return Order.IsValidated;
        }

        public async Task<bool> Approve(Order Order)
        {
            if (await ValidateId(Order))
            {
                if(Order.StatusId != Enums.OrderStatusEnum.PENDING.Id)
                {
                    if(Order.StatusId == Enums.OrderStatusEnum.DONE.Id)
                        Order.AddError(nameof(OrderValidator), nameof(Order.StatusId), ErrorCode.OrderDone);
                    if (Order.StatusId == Enums.OrderStatusEnum.APPROVED.Id)
                        Order.AddError(nameof(OrderValidator), nameof(Order.StatusId), ErrorCode.OrderApproved);
                    if (Order.StatusId == Enums.OrderStatusEnum.REJECTED.Id)
                        Order.AddError(nameof(OrderValidator), nameof(Order.StatusId), ErrorCode.OrderReject);
                }

                if(Order.Reservations == null || !Order.Reservations.Any())
                    Order.AddError(nameof(OrderValidator), nameof(Order.Reservations), ErrorCode.ReservationsEmpty);
                else if(Order.NumOfTable != Order.Reservations.Count())
                    Order.AddError(nameof(OrderValidator), nameof(Order.Reservations), ErrorCode.ReservationsInvalid);
            }
            return Order.IsValidated;
        }

        public async Task<bool> Reject(Order Order)
        {
            if (await ValidateId(Order))
            {
                if (Order.StatusId != Enums.OrderStatusEnum.PENDING.Id)
                {
                    if (Order.StatusId == Enums.OrderStatusEnum.DONE.Id)
                        Order.AddError(nameof(OrderValidator), nameof(Order.StatusId), ErrorCode.OrderDone);
                    if (Order.StatusId == Enums.OrderStatusEnum.APPROVED.Id)
                        Order.AddError(nameof(OrderValidator), nameof(Order.StatusId), ErrorCode.OrderApproved);
                    if (Order.StatusId == Enums.OrderStatusEnum.REJECTED.Id)
                        Order.AddError(nameof(OrderValidator), nameof(Order.StatusId), ErrorCode.OrderReject);
                }
            }
            return Order.IsValidated;
        }

        public async Task<bool> Done(Order Order)
        {
            if (await ValidateId(Order))
            {
                if (await ValidateId(Order))
                {
                    if (Order.StatusId != Enums.OrderStatusEnum.APPROVED.Id)
                    {
                        if (Order.StatusId == Enums.OrderStatusEnum.DONE.Id)
                            Order.AddError(nameof(OrderValidator), nameof(Order.StatusId), ErrorCode.OrderDone);
                        if (Order.StatusId == Enums.OrderStatusEnum.PENDING.Id)
                            Order.AddError(nameof(OrderValidator), nameof(Order.StatusId), ErrorCode.OrderPending);
                        if (Order.StatusId == Enums.OrderStatusEnum.REJECTED.Id)
                            Order.AddError(nameof(OrderValidator), nameof(Order.StatusId), ErrorCode.OrderReject);
                    }
                }
            }
            return Order.IsValidated;
        }

        public async Task<bool> BulkDelete(List<Order> Orders)
        {
            return true;
        }
        
        public async Task<bool> Import(List<Order> Orders)
        {
            return true;
        }
    }
}
