using Common;
using Helpers;
using MM.Entities;
using MM.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Services.MOrder
{
    public interface IOrderService :  IServiceScoped
    {
        Task<int> Count(OrderFilter OrderFilter);
        Task<List<Order>> List(OrderFilter OrderFilter);
        Task<Order> Get(long Id);
        Task<Order> Create(Order Order);
        Task<Order> Update(Order Order);
        Task<Order> Approve(Order Order);
        Task<Order> Reject(Order Order);
        Task<Order> Done(Order Order);
        Task<Order> Delete(Order Order);
    }

    public class OrderService : IOrderService
    {
        private IUOW UOW;
        private ICurrentContext CurrentContext;
        private IOrderValidator OrderValidator;

        public OrderService(
            IUOW UOW,
            ICurrentContext CurrentContext,
            IOrderValidator OrderValidator
        )
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
            this.OrderValidator = OrderValidator;
        }
        public async Task<int> Count(OrderFilter OrderFilter)
        {
            try
            {
                int result = await UOW.OrderRepository.Count(OrderFilter);
                return result;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<List<Order>> List(OrderFilter OrderFilter)
        {
            try
            {
                List<Order> Orders = await UOW.OrderRepository.List(OrderFilter);
                return Orders;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }
        public async Task<Order> Get(long Id)
        {
            Order Order = await UOW.OrderRepository.Get(Id);
            if (Order == null)
                return null;
            return Order;
        }
       
        public async Task<Order> Create(Order Order)
        {
            if (!await OrderValidator.Create(Order))
                return Order;

            try
            {
                await UOW.Begin();
                var code = await UOW.OrderRepository.Count(new OrderFilter { }) + 1;
                Order.Code = $"#{code}";
                Order.StatusId = Enums.OrderStatusEnum.PENDING.Id;
                if (Order.OrderContents != null)
                {
                    FoodFoodTypeMappingFilter FoodFoodTypeMappingFilter = new FoodFoodTypeMappingFilter
                    {
                        Skip = 0,
                        Take = int.MaxValue,
                        Selects = FoodFoodTypeMappingSelect.ALL,
                    };
                    List<FoodFoodTypeMapping> FoodFoodTypeMappings = await UOW.FoodFoodTypeMappingRepository.List(FoodFoodTypeMappingFilter);
                    foreach (var OrderContent in Order.OrderContents)
                    {
                        OrderContent.Code = Order.Code + Order.OrderContents.IndexOf(OrderContent).ToString();
                        FoodFoodTypeMapping FoodFoodTypeMapping = FoodFoodTypeMappings.Where(x => x.FoodId == OrderContent.FoodFoodTypeMapping.FoodId && x.FoodTypeId == OrderContent.FoodFoodTypeMapping.FoodTypeId).FirstOrDefault();
                        OrderContent.FoodFoodTypeMappingId = FoodFoodTypeMapping.Id;
                        OrderContent.FoodFoodTypeMapping = FoodFoodTypeMapping;
                    }
                }
                await Calculator(Order);
                await UOW.OrderRepository.Create(Order);
                await UOW.Commit();

                return await UOW.OrderRepository.Get(Order.Id);
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Order> Update(Order Order)
        {
            if (!await OrderValidator.Update(Order))
                return Order;
            try
            {
                var oldData = await UOW.OrderRepository.Get(Order.Id);

                await UOW.Begin();
                if (Order.OrderContents != null)
                {
                    FoodFoodTypeMappingFilter FoodFoodTypeMappingFilter = new FoodFoodTypeMappingFilter
                    {
                        Skip = 0,
                        Take = int.MaxValue,
                        Selects = FoodFoodTypeMappingSelect.ALL,
                    };
                    List<FoodFoodTypeMapping> FoodFoodTypeMappings = await UOW.FoodFoodTypeMappingRepository.List(FoodFoodTypeMappingFilter);
                    foreach (var OrderContent in Order.OrderContents)
                    {
                        OrderContent.Code = Order.Code + Order.OrderContents.IndexOf(OrderContent).ToString();
                        FoodFoodTypeMapping FoodFoodTypeMapping = FoodFoodTypeMappings.Where(x => x.FoodId == OrderContent.FoodFoodTypeMapping.FoodId && x.FoodTypeId == OrderContent.FoodFoodTypeMapping.FoodTypeId).FirstOrDefault();
                        OrderContent.FoodFoodTypeMappingId = FoodFoodTypeMapping.Id;
                        OrderContent.FoodFoodTypeMapping = FoodFoodTypeMapping;
                    }
                }
                await Calculator(Order);
                await UOW.OrderRepository.Update(Order);
                await UOW.Commit();

                var newData = await UOW.OrderRepository.Get(Order.Id);
                return newData;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Order> Approve(Order Order)
        {
            if (!await OrderValidator.Approve(Order))
                return Order;
            try
            {
                var oldData = await UOW.OrderRepository.Get(Order.Id);
                oldData.StatusId = Enums.OrderStatusEnum.APPROVED.Id;
                if(Order.Reservations != null && Order.Reservations.Any())
                {
                    foreach (var Reservation in Order.Reservations)
                    {
                        Reservation.StatusId = Enums.ReservationStatusEnum.BUSY.Id;
                        Reservation.OrderId = Order.Id;
                    }
                    await UOW.ReservationRepository.BulkUpdate(Order.Reservations);
                }
                await UOW.OrderRepository.Update(oldData);
                await UOW.Commit();

                var newData = await UOW.OrderRepository.Get(Order.Id);
                return newData;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Order> Reject(Order Order)
        {
            if (!await OrderValidator.Reject(Order))
                return Order;
            try
            {
                var oldData = await UOW.OrderRepository.Get(Order.Id);
                oldData.StatusId = Enums.OrderStatusEnum.REJECTED.Id;
                await UOW.OrderRepository.Update(oldData);
                await UOW.Commit();

                var newData = await UOW.OrderRepository.Get(Order.Id);
                return newData;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Order> Done(Order Order)
        {
            if (!await OrderValidator.Done(Order))
                return Order;
            try
            {
                var oldData = await UOW.OrderRepository.Get(Order.Id);
                oldData.StatusId = Enums.OrderStatusEnum.DONE.Id;
                if (Order.Reservations != null && Order.Reservations.Any())
                {
                    foreach (var Reservation in Order.Reservations)
                    {
                        Reservation.StatusId = Enums.ReservationStatusEnum.EMPTY.Id;
                        Reservation.OrderId = Order.Id;
                    }
                    await UOW.ReservationRepository.BulkUpdate(Order.Reservations);
                }
                await UOW.OrderRepository.Update(oldData);
                await UOW.Commit();

                var newData = await UOW.OrderRepository.Get(Order.Id);
                return newData;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Order> Delete(Order Order)
        {
            if (!await OrderValidator.Delete(Order))
                return Order;

            try
            {
                await UOW.Begin();
                await UOW.OrderRepository.Delete(Order);
                await UOW.Commit();
                return Order;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        private async Task<Order> Calculator(Order Order)
        {
            if(Order.OrderContents != null && Order.OrderContents.Any())
            {
                Order.SubTotal = 0;
                Order.Total = 0;
                foreach (var OrderContent in Order.OrderContents)
                {
                    OrderContent.Amount = OrderContent.FoodFoodTypeMapping.Food.PriceEach * OrderContent.Quantity;

                    if (OrderContent.FoodFoodTypeMapping.FoodTypeId == Enums.FoodTypeEnums.MEDIUM.Id)
                        OrderContent.Amount = 120 * OrderContent.Amount / 100;
                    if (OrderContent.FoodFoodTypeMapping.FoodTypeId == Enums.FoodTypeEnums.LARGE.Id)
                        OrderContent.Amount = 150 * OrderContent.Amount / 100;
                    Order.SubTotal += OrderContent.Amount;
                    OrderContent.Amount = OrderContent.Amount - OrderContent.Amount * OrderContent.FoodFoodTypeMapping.Food.DiscountRate / 100;
                    Order.Total += OrderContent.Amount;
                }
            }
            return Order;
        }
    }
}
