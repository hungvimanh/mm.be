using Common;
using Helpers;
using MM.Entities;
using MM.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MM.Services.MFoodGrouping
{
    public interface IFoodGroupingService :  IServiceScoped
    {
        Task<int> Count(FoodGroupingFilter FoodGroupingFilter);
        Task<List<FoodGrouping>> List(FoodGroupingFilter FoodGroupingFilter);
        Task<FoodGrouping> Get(long Id);
        Task<FoodGrouping> Create(FoodGrouping FoodGrouping);
        Task<FoodGrouping> Update(FoodGrouping FoodGrouping);
        Task<FoodGrouping> Delete(FoodGrouping FoodGrouping);
    }

    public class FoodGroupingService : IFoodGroupingService
    {
        private IUOW UOW;
        private ICurrentContext CurrentContext;
        private IFoodGroupingValidator FoodGroupingValidator;

        public FoodGroupingService(
            IUOW UOW,
            ICurrentContext CurrentContext,
            IFoodGroupingValidator FoodGroupingValidator
        )
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
            this.FoodGroupingValidator = FoodGroupingValidator;
        }
        public async Task<int> Count(FoodGroupingFilter FoodGroupingFilter)
        {
            try
            {
                int result = await UOW.FoodGroupingRepository.Count(FoodGroupingFilter);
                return result;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<List<FoodGrouping>> List(FoodGroupingFilter FoodGroupingFilter)
        {
            try
            {
                List<FoodGrouping> FoodGroupings = await UOW.FoodGroupingRepository.List(FoodGroupingFilter);
                return FoodGroupings;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }
        public async Task<FoodGrouping> Get(long Id)
        {
            FoodGrouping FoodGrouping = await UOW.FoodGroupingRepository.Get(Id);
            if (FoodGrouping == null)
                return null;
            return FoodGrouping;
        }
       
        public async Task<FoodGrouping> Create(FoodGrouping FoodGrouping)
        {
            if (!await FoodGroupingValidator.Create(FoodGrouping))
                return FoodGrouping;

            try
            {
                await UOW.Begin();
                await UOW.FoodGroupingRepository.Create(FoodGrouping);
                await UOW.Commit();

                return await UOW.FoodGroupingRepository.Get(FoodGrouping.Id);
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<FoodGrouping> Update(FoodGrouping FoodGrouping)
        {
            if (!await FoodGroupingValidator.Update(FoodGrouping))
                return FoodGrouping;
            try
            {
                var oldData = await UOW.FoodGroupingRepository.Get(FoodGrouping.Id);

                await UOW.Begin();
                await UOW.FoodGroupingRepository.Update(FoodGrouping);
                await UOW.Commit();

                var newData = await UOW.FoodGroupingRepository.Get(FoodGrouping.Id);
                return newData;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<FoodGrouping> Delete(FoodGrouping FoodGrouping)
        {
            if (!await FoodGroupingValidator.Delete(FoodGrouping))
                return FoodGrouping;

            try
            {
                await UOW.Begin();
                await UOW.FoodGroupingRepository.Delete(FoodGrouping);
                await UOW.Commit();
                return FoodGrouping;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

    }
}
