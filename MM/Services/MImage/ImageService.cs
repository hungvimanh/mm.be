﻿using Common;
using Helpers;
using MM.Entities;
using MM.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MM.Services.MImage
{
    public interface IImageService : IServiceScoped
    {
        Task<List<Image>> List(ImageFilter ImageFilter);
        Task<Image> Download(long Id);
        Task<Image> Upload(Image File);
    }
    public class ImageService : IImageService
    {
        private IUOW UOW;
        public ImageService(IUOW UOW)
        {
            this.UOW = UOW;
        }

        public async Task<Image> Upload(Image Image)
        {
            FileInfo fileInfo = new FileInfo(Image.Name);
            string path = $"/food/{StaticParams.DateTimeNow.ToString("yyyyMMdd")}/{Guid.NewGuid()}{fileInfo.Extension}";
            Image.Path = path;
            Image = await Create(Image, path);
            return Image;
        }
        private async Task<Image> Create(Image Image, string path)
        {
            Image.Path = Image.Path.ToLower();
            try
            {
                Image.Url = "/api/image/download" + path;
                await UOW.Begin();
                await UOW.ImageRepository.Create(Image);
                await UOW.Commit();
                Image = await UOW.ImageRepository.Get(Image.Id);
                return Image;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Image> Download(long Id)
        {
            return await UOW.ImageRepository.Get(Id);
        }

        public async Task<List<Image>> List(ImageFilter ImageFilter)
        {
            return await UOW.ImageRepository.List(ImageFilter);
        }
    }
}
