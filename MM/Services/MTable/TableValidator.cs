using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using MM.Entities;
using MM;
using MM.Repositories;

namespace MM.Services.MTable
{
    public interface ITableValidator : IServiceScoped
    {
        Task<bool> Create(Table Table);
        Task<bool> Update(Table Table);
        Task<bool> Delete(Table Table);
        Task<bool> BulkDelete(List<Table> Tables);
        Task<bool> Import(List<Table> Tables);
    }

    public class TableValidator : ITableValidator
    {
        public enum ErrorCode
        {
            IdNotExisted,
            CodeEmpty,
            CodeOverLenth,
            CodeExisted,
            StatusIdNotExisted
        }

        private IUOW UOW;
        private ICurrentContext CurrentContext;

        public TableValidator(IUOW UOW, ICurrentContext CurrentContext)
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
        }

        public async Task<bool> ValidateId(Table Table)
        {
            TableFilter TableFilter = new TableFilter
            {
                Skip = 0,
                Take = 10,
                Id = new IdFilter { Equal = Table.Id },
                Selects = TableSelect.Id
            };

            int count = await UOW.TableRepository.Count(TableFilter);
            if (count == 0)
                Table.AddError(nameof(TableValidator), nameof(Table.Id), ErrorCode.IdNotExisted);
            return count == 1;
        }

        public async Task<bool> ValidateCode(Table Table)
        {
            if(string.IsNullOrWhiteSpace(Table.Code))
                Table.AddError(nameof(TableValidator), nameof(Table.Code), ErrorCode.CodeEmpty);
            else
            {
                if(Table.Code.Length > 255)
                    Table.AddError(nameof(TableValidator), nameof(Table.Code), ErrorCode.CodeOverLenth);
                TableFilter filter = new TableFilter
                {
                    Code = new StringFilter { Equal = Table.Code },
                    Id = new IdFilter { NotEqual = Table.Id }
                };
                int count = await UOW.TableRepository.Count(filter);
                if(count >= 0)
                    Table.AddError(nameof(TableValidator), nameof(Table.Code), ErrorCode.CodeExisted);
            }
            return Table.IsValidated;
        }

        public async Task<bool> ValidateStatus(Table Table)
        {
            if (Table.StatusId != Enums.StatusEnum.ACTIVE.Id && Table.StatusId != Enums.StatusEnum.INACTIVE.Id)
                Table.AddError(nameof(TableValidator), nameof(Table.StatusId), ErrorCode.StatusIdNotExisted);
            return Table.IsValidated;
        }

        public async Task<bool>Create(Table Table)
        {
            await ValidateCode(Table);
            await ValidateStatus(Table);
            return Table.IsValidated;
        }

        public async Task<bool> Update(Table Table)
        {
            if (await ValidateId(Table))
            {
            }
            return Table.IsValidated;
        }

        public async Task<bool> Delete(Table Table)
        {
            if (await ValidateId(Table))
            {
            }
            return Table.IsValidated;
        }
        
        public async Task<bool> BulkDelete(List<Table> Tables)
        {
            return true;
        }
        
        public async Task<bool> Import(List<Table> Tables)
        {
            return true;
        }
    }
}
