using Common;
using Helpers;
using MM.Entities;
using MM.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MM.Services.MFoodType
{
    public interface IFoodTypeService :  IServiceScoped
    {
        Task<int> Count(FoodTypeFilter FoodTypeFilter);
        Task<List<FoodType>> List(FoodTypeFilter FoodTypeFilter);
        Task<FoodType> Get(long Id);
        Task<FoodType> Create(FoodType FoodType);
        Task<FoodType> Update(FoodType FoodType);
        Task<FoodType> Delete(FoodType FoodType);
    }

    public class FoodTypeService : IFoodTypeService
    {
        private IUOW UOW;
        private ICurrentContext CurrentContext;
        private IFoodTypeValidator FoodTypeValidator;

        public FoodTypeService(
            IUOW UOW,
            ICurrentContext CurrentContext,
            IFoodTypeValidator FoodTypeValidator
        )
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
            this.FoodTypeValidator = FoodTypeValidator;
        }
        public async Task<int> Count(FoodTypeFilter FoodTypeFilter)
        {
            try
            {
                int result = await UOW.FoodTypeRepository.Count(FoodTypeFilter);
                return result;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<List<FoodType>> List(FoodTypeFilter FoodTypeFilter)
        {
            try
            {
                List<FoodType> FoodTypes = await UOW.FoodTypeRepository.List(FoodTypeFilter);
                return FoodTypes;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }
        public async Task<FoodType> Get(long Id)
        {
            FoodType FoodType = await UOW.FoodTypeRepository.Get(Id);
            if (FoodType == null)
                return null;
            return FoodType;
        }
       
        public async Task<FoodType> Create(FoodType FoodType)
        {
            if (!await FoodTypeValidator.Create(FoodType))
                return FoodType;

            try
            {
                await UOW.Begin();
                await UOW.FoodTypeRepository.Create(FoodType);
                await UOW.Commit();

                return await UOW.FoodTypeRepository.Get(FoodType.Id);
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<FoodType> Update(FoodType FoodType)
        {
            if (!await FoodTypeValidator.Update(FoodType))
                return FoodType;
            try
            {
                var oldData = await UOW.FoodTypeRepository.Get(FoodType.Id);

                await UOW.Begin();
                await UOW.FoodTypeRepository.Update(FoodType);
                await UOW.Commit();

                var newData = await UOW.FoodTypeRepository.Get(FoodType.Id);
                return newData;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<FoodType> Delete(FoodType FoodType)
        {
            if (!await FoodTypeValidator.Delete(FoodType))
                return FoodType;

            try
            {
                await UOW.Begin();
                await UOW.FoodTypeRepository.Delete(FoodType);
                await UOW.Commit();
                return FoodType;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

    }
}
