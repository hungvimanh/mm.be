using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using MM.Entities;
using MM;
using MM.Repositories;

namespace MM.Services.MNotification
{
    public interface INotificationValidator : IServiceScoped
    {
        Task<bool> Create(Notification Notification);
        Task<bool> Update(Notification Notification);
        Task<bool> Delete(Notification Notification);
        Task<bool> BulkDelete(List<Notification> Notifications);
        Task<bool> Import(List<Notification> Notifications);
    }

    public class NotificationValidator : INotificationValidator
    {
        public enum ErrorCode
        {
            IdNotExisted,
        }

        private IUOW UOW;
        private ICurrentContext CurrentContext;

        public NotificationValidator(IUOW UOW, ICurrentContext CurrentContext)
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
        }

        public async Task<bool> ValidateId(Notification Notification)
        {
            NotificationFilter NotificationFilter = new NotificationFilter
            {
                Skip = 0,
                Take = 10,
                Id = new IdFilter { Equal = Notification.Id },
                Selects = NotificationSelect.Id
            };

            int count = await UOW.NotificationRepository.Count(NotificationFilter);
            if (count == 0)
                Notification.AddError(nameof(NotificationValidator), nameof(Notification.Id), ErrorCode.IdNotExisted);
            return count == 1;
        }

        public async Task<bool>Create(Notification Notification)
        {
            return Notification.IsValidated;
        }

        public async Task<bool> Update(Notification Notification)
        {
            if (await ValidateId(Notification))
            {
            }
            return Notification.IsValidated;
        }

        public async Task<bool> Delete(Notification Notification)
        {
            if (await ValidateId(Notification))
            {
            }
            return Notification.IsValidated;
        }
        
        public async Task<bool> BulkDelete(List<Notification> Notifications)
        {
            foreach (Notification Store in Notifications)
            {
                await Delete(Store);
            }
            return Notifications.All(st => st.IsValidated);
        }
        
        public async Task<bool> Import(List<Notification> Notifications)
        {
            return true;
        }
    }
}
