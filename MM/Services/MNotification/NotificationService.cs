using Common;
using Helpers;
using MM.Entities;
using MM.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MM.Services.MNotification
{
    public interface INotificationService :  IServiceScoped
    {
        Task<int> Count(NotificationFilter NotificationFilter);
        Task<List<Notification>> List(NotificationFilter NotificationFilter);
        Task<Notification> Get(long Id);
        Task<Notification> Create(Notification Notification);
        Task<Notification> Update(Notification Notification);
        Task<Notification> Delete(Notification Notification);
        Task<List<Notification>> BulkDelete(List<Notification> Notifications);
    }

    public class NotificationService : INotificationService
    {
        private IUOW UOW;
        private ICurrentContext CurrentContext;
        private INotificationValidator NotificationValidator;

        public NotificationService(
            IUOW UOW,
            ICurrentContext CurrentContext,
            INotificationValidator NotificationValidator
        )
        {
            this.UOW = UOW;
            this.CurrentContext = CurrentContext;
            this.NotificationValidator = NotificationValidator;
        }
        public async Task<int> Count(NotificationFilter NotificationFilter)
        {
            try
            {
                int result = await UOW.NotificationRepository.Count(NotificationFilter);
                return result;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<List<Notification>> List(NotificationFilter NotificationFilter)
        {
            try
            {
                List<Notification> Notifications = await UOW.NotificationRepository.List(NotificationFilter);
                return Notifications;
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }
        public async Task<Notification> Get(long Id)
        {
            Notification Notification = await UOW.NotificationRepository.Get(Id);
            if (Notification == null)
                return null;
            return Notification;
        }
       
        public async Task<Notification> Create(Notification Notification)
        {
            if (!await NotificationValidator.Create(Notification))
                return Notification;

            try
            {
                Notification.Time = StaticParams.DateTimeNow;
                Notification.AccountId = CurrentContext.AccountId;
                Notification.Unread = false;
                await UOW.Begin();
                await UOW.NotificationRepository.Create(Notification);
                await UOW.Commit();

                return await UOW.NotificationRepository.Get(Notification.Id);
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Notification> Update(Notification Notification)
        {
            if (!await NotificationValidator.Update(Notification))
                return Notification;
            try
            {
                var oldData = await UOW.NotificationRepository.Get(Notification.Id);
                oldData.Unread = Notification.Unread;
                await UOW.Begin();
                await UOW.NotificationRepository.Update(oldData);
                await UOW.Commit();

                var newData = await UOW.NotificationRepository.Get(Notification.Id);
                return newData;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<Notification> Delete(Notification Notification)
        {
            if (!await NotificationValidator.Delete(Notification))
                return Notification;

            try
            {
                await UOW.Begin();
                await UOW.NotificationRepository.Delete(Notification);
                await UOW.Commit();
                return Notification;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }

        public async Task<List<Notification>> BulkDelete(List<Notification> Notifications)
        {
            if (!await NotificationValidator.BulkDelete(Notifications))
                return Notifications;

            try
            {
                await UOW.Begin();
                await UOW.NotificationRepository.BulkDelete(Notifications);
                await UOW.Commit();
                return Notifications;
            }
            catch (Exception ex)
            {
                await UOW.Rollback();
                if (ex.InnerException == null)
                    throw new MessageException(ex);
                else
                    throw new MessageException(ex.InnerException);
            }
        }
    }
}
